(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
// load global event model
require('models/event.js');

// load searcher module
require('searcher/searcher.js');
},{"models/event.js":6,"searcher/searcher.js":10}],2:[function(require,module,exports){
require('./helpers.js');

// baseClass
// This is a modified version of John Resig's class
// http://ejohn.org/blog/simple-javascript-inheritance/
// It provides class level inheritance and callbacks.
//
// =============== HELPERS =================
// if we are initializing a new class
var initializing = false;
var makeArray = $.makeArray;
var isFunction = $.isFunction;
var isArray = $.isArray;
var extend = $.extend;
var getObject = $.String.getObject;

var concatArgs = function(arr, args) {
    return arr.concat(makeArray(args));
};

// tests if we can get super in .toString()
var fnTest = /xyz/.test(function() {
        xyz;
    }) ? /\b_super\b/ : /.*/;

// overwrites an object with methods, sets up _super
//   newProps - new properties
//   oldProps - where the old properties might be
//   addTo - what we are adding to
var inheritProps = function(newProps, oldProps, addTo) {
    addTo = addTo || newProps;

    for (var name in newProps) {
        // Check if we're overwriting an existing function
        addTo[name] = isFunction(newProps[name]) &&
                      isFunction(oldProps[name]) &&
                      fnTest.test(newProps[name]) ? (function( name, fn ) {
            return function() {
                var tmp = this._super,
                    ret;

                // Add a new ._super() method that is the same method
                // but on the super-class
                this._super = oldProps[name];

                // The method only need to be bound temporarily, so we
                // remove it when we're done executing
                ret = fn.apply(this, arguments);
                this._super = tmp;
                return ret;
            };
        })(name, newProps[name]) : newProps[name];
    }
};

var STR_PROTOTYPE = 'prototype';

/**
 * @class baseClass
 */
var baseClass = function() {
    if (arguments.length) {
        return baseClass.extend.apply(baseClass, arguments);
    }
};

/* @Static*/
extend(baseClass, {
    proxy: function(funcs) {

        //args that should be curried
        var args = makeArray(arguments),
            self;

        // get the functions to callback
        funcs = args.shift();

        // if there is only one function, make funcs into an array
        if (!isArray(funcs) ) {
            funcs = [funcs];
        }

        // keep a reference to us in self
        self = this;

        for(var i =0; i< funcs.length; i++) {
            if(typeof funcs[i] == 'string' && !isFunction(this[funcs[i]])) {
                throw ('class.js '+( this.fullName || this.Class.fullName)+' does not have a '+funcs[i]+'method!');
            }
        }

        return function class_cb() {
            // add the arguments after the curried args
            var cur = concatArgs(args, arguments),
                isString,
                length = funcs.length,
                f = 0,
                func;

            // go through each function to call back
            for (; f < length; f++ ) {
                func = funcs[f];
                if (!func ) {
                    continue;
                }

                // set called with the name of the function on self (this is how this.view works)
                isString = typeof func == 'string';
                if ( isString && self._set_called ) {
                    self.called = func;
                }

                // call the function
                cur = (isString ? self[func] : func).apply(self, cur || []);

                // pass the result to the next function (if there is a next function)
                if ( f < length - 1 ) {
                    cur = !isArray(cur) || cur._use_call ? [cur] : cur
                }
            }
            return cur;
        }
    },

    newInstance: function() {
        // get a raw instance objet (init is not called)
        var inst = this.rawInstance(),
            args;

        // call setup if there is a setup
        if ( inst.setup ) {
            args = inst.setup.apply(inst, arguments);
        }
        // call init if there is an init, if setup returned args, use those as the arguments
        if ( inst.init ) {
            inst.init.apply(inst, isArray(args) ? args : arguments);
        }
        return inst;
    },

    setup: function( baseClass, fullName ) {
        // set defaults as the merger of the parent defaults and this object's defaults
        this.defaults = extend(true, {}, baseClass.defaults, this.defaults);
        return arguments;
    },
    rawInstance: function() {
        // prevent running init
        initializing = true;
        var inst = new this();
        initializing = false;
        // allow running init
        return inst;
    },

    extend: function( fullName, klass, proto ) {
        // figure out what was passed and normalize it
        if ( typeof fullName != 'string' ) {
            proto = klass;
            klass = fullName;
            fullName = null;
        }
        if (!proto ) {
            proto = klass;
            klass = null;
        }

        proto = proto || {};
        var _super_class = this,
            _super = this[STR_PROTOTYPE],
            name, shortName, namespace, prototype;

        // Instantiate a base class (but only create the instance,
        // don't run the init constructor)
        initializing = true;
        prototype = new this();
        initializing = false;

        // Copy the properties over onto the new prototype
        inheritProps(proto, _super, prototype);

        // The dummy class constructor
        function Class() {
            // All construction is actually done in the init method
            if ( initializing ) return;

            // we are being called w/o new, we are extending
            if ( this.constructor !== Class && arguments.length ) {
                return arguments.callee.extend.apply(arguments.callee, arguments)
            } else { //we are being called w/ new
                return this.Class.newInstance.apply(this.Class, arguments)
            }
        }

        // Copy old stuff onto class
        for (name in this) {
            if (this.hasOwnProperty(name)) {
                Class[name] = this[name];
            }
        }

        // copy new static props on class
        inheritProps(klass, this, Class);

        // do namespace stuff
        if (fullName) {
            var parts = fullName.split(/\./),
                shortName = parts.pop(),
                current = getObject(parts.join('.'), window, true),
                namespace = current;

            if(current[shortName]) {
                throw ('baseClass.js There\'s already something called ' + fullName)
            }

            //!steal-remove-end
            current[shortName] = Class;
        }

        // set things that can't be overwritten
        extend(Class, {
            prototype: prototype,
            /**
             * @attribute namespace
             * The namespaces object
             *
             *     $.Class('MyOrg.MyClass',{},{})
             *     MyOrg.MyClass.namespace //-> MyOrg
             *
             */
            namespace: namespace,
            /**
             * @attribute shortName
             * The name of the class without its namespace, provided for introspection purposes.
             *
             *     $.Class('MyOrg.MyClass',{},{})
             *     MyOrg.MyClass.shortName //-> 'MyClass'
             *     MyOrg.MyClass.fullName //->  'MyOrg.MyClass'
             *
             */
            shortName: shortName,
            constructor: Class,
            /**
             * @attribute fullName
             * The full name of the class, including namespace, provided for introspection purposes.
             *
             *     $.Class('MyOrg.MyClass',{},{})
             *     MyOrg.MyClass.shortName //-> 'MyClass'
             *     MyOrg.MyClass.fullName //->  'MyOrg.MyClass'
             *
             */
            fullName: fullName
        });

        //make sure our prototype looks nice
        Class[STR_PROTOTYPE].Class = Class[STR_PROTOTYPE].constructor = Class;

        // call the class setup
        var args = Class.setup.apply(Class, concatArgs([_super_class], arguments));

        // call the class init
        if (Class.init) {
            Class.init.apply(Class, args || concatArgs([_super_class], arguments));
        }

        /* @Prototype*/
        return Class;
    }
});

baseClass.callback = baseClass[STR_PROTOTYPE].callback = baseClass[STR_PROTOTYPE].proxy = baseClass.proxy;

// export base class
module.exports = baseClass;
},{"./helpers.js":5}],3:[function(require,module,exports){
var Class = require('./baseClass.js');

module.exports = Class(
/* @static */
{
	setup : function() {
		this._super.apply(this, arguments);
	}
},
/* @prototype */
{
	setup : function(el, options) {
		el = (typeof el == 'string' ? $(el) :
				(el.jquery ? el : [el]))[0];

		this.scope = $(el);
		this.options = $.extend($.extend(true, {}, this.Class.defaults), options);

		return [this.scope, this.options].concat($.makeArray(arguments).slice(2));
	}
});
},{"./baseClass.js":2}],4:[function(require,module,exports){
// load string plugin
require('./helpers.js');
var Class = require('./baseClass.js');

// Common helper methods taken from jQuery (or other places)
// Keep here so someday can be abstracted
var $String = $.String,
	getObject = $String.getObject,
	underscore = $String.underscore,
	classize = $String.classize,
	isArray = $.isArray,
	makeArray = $.makeArray,
	extend = $.extend,
	each = $.each,

	trigger = function(obj, event, args) {
		$.event.trigger(event, args, obj, true)
	},

	// takes attrs, and adds it to the attrs (so it can be added to the url)
	// if attrs already has an id, it means it's trying to update the id
	// in this case, it sets the new ID as newId.
	addId = function( model, attrs, id ) {
		attrs = attrs || {};
		var identity = model.id;
		if ( attrs[identity] && attrs[identity] !== id ) {
			attrs['new' + $String.capitalize(id)] = attrs[identity];
			delete attrs[identity];
		}
		attrs[identity] = id;
		return attrs;
	},

	// returns the best list-like object (list is passed)
	getList = function( type ) {
		var listType = type || $.Model.List || Array;
		return new listType();
	},

	// a helper function for getting an id from an instance
	getId = function( inst ) {
		return inst[inst.constructor.id]
	},

	// returns a collection of unique items
	// this works on objects by adding a '__u Nique' property.
	unique = function( items ) {
		var collect = [];
		// check unique property, if it isn't there, add to collect
		each(items, function( i, item ) {
			if (!item['__u Nique'] ) {
				collect.push(item);
				item['__u Nique'] = 1;
			}
		});
		// remove unique
		return each(collect, function( i, item ) {
			delete item['__u Nique'];
		});
	},

	// helper makes a request to a static ajax method
	// it also calls updated, created, or destroyed
	// and it returns a deferred that resolvesWith self and the data
	// returned from the ajax request
	makeRequest = function( self, type, success, error, method ) {
		// create the deferred makeRequest will return
		var deferred = $.Deferred(),
			// on a successful ajax request, call the
			// updated | created | destroyed method
			// then resolve the deferred
			resolve = function( data ) {
				self[method || type + 'd'](data);
				deferred.resolveWith(self, [self, data, type]);
			},
			// on reject reject the deferred
			reject = function( data ) {
				deferred.rejectWith(self, [data])
			},
			// the args to pass to the ajax method
			args = [self.serialize(), resolve, reject],
			// the Model
			model = self.constructor,
			jqXHR,
			promise = deferred.promise();

		// destroy does not need data
		if ( type == 'destroy' ) {
			args.shift();
		}

		// update and destroy need the id
		if ( type !== 'create' ) {
			args.unshift(getId(self))
		}

		// hook up success and error
		deferred.then(success);
		deferred.fail(error);

		// call the model's function and hook up
		// abort
		jqXHR = model[type].apply(model, args);
		if(jqXHR && jqXHR.abort){
			promise.abort = function(){
				jqXHR.abort();
			}
		}
		return promise;
	},
	// a quick way to tell if it's an object and not some string
	isObject = function( obj ) {
		return typeof obj === 'object' && obj !== null && obj;
	},
	$method = function( name ) {
		return function( eventType, handler ) {
			return $.fn[name].apply($([this]), arguments);
		}
	},
	bind = $method('bind'),
	unbind = $method('unbind'),
	STR_CONSTRUCTOR = 'constructor';

/**
 * Base Model Setup
 */
var baseModel = Class({
	setup: function( superClass, stat, proto ) {

		var self = this,
			fullName = this.fullName;
		//we do not inherit attributes (or validations)
		each(['attributes', 'validations'], function( i, name ) {
			if (!self[name] || superClass[name] === self[name] ) {
				self[name] = {};
			}
		})

		//add missing converters and serializes
		each(['convert', 'serialize'], function( i, name ) {
			if ( superClass[name] != self[name] ) {
				self[name] = extend({}, superClass[name], self[name]);
			}
		});

		this._fullName = underscore(fullName.replace(/\./g, '_'));
		this._shortName = underscore(this.shortName);

		if ( fullName.indexOf('jQuery') == 0 ) {
			return;
		}

		//add this to the collection of models
		//$.Model.models[this._fullName] = this;
		if ( this.listType ) {
			this.list = new this.listType([]);
		}
		//!steal-remove-start
		if (!proto ) {
			steal.dev.warn('model.js ' + fullName + ' has no static properties.  You probably need  ,{} ')
		}
		//!steal-remove-end
		each(ajaxMethods, function(name, method){
			var prop = self[name];
			if ( typeof prop !== 'function' ) {
				self[name] = method(prop);
			}
		});

		//add ajax converters
		var converters = {},
			convertName = '* ' + this._shortName + '.model';

		converters[convertName + 's'] = this.proxy('models');
		converters[convertName] = this.proxy('model');

		$.ajaxSetup({
			converters: converters
		});
	},
	/**
	 * @attribute attributes
	 * Attributes contains a map of attribute names/types.
	 * You can use this in conjunction with
	 * [$.Model.convert] to provide automatic
	 * [jquery.model.typeconversion type conversion] (including
	 * associations).
	 *
	 * The following converts dueDates to JavaScript dates:
	 *
	 *
	 *     $.Model('Contact',{
	 *       attributes : {
	 *         birthday : 'date'
	 *       },
	 *       convert : {
	 *         date : function(raw){
	 *           if(typeof raw == 'string'){
	 *             var matches = raw.match(/(\d+)-(\d+)-(\d+)/)
	 *             return new Date( matches[1],
	 *                      (+matches[2])-1,
	 *                     matches[3] )
	 *           }else if(raw instanceof Date){
	 *               return raw;
	 *           }
	 *         }
	 *       }
	 *     },{})
	 *
	 * ## Associations
	 *
	 * Attribute type values can also represent the name of a
	 * function.  The most common case this is used is for
	 * associated data.
	 *
	 * For example, a Deliverable might have many tasks and
	 * an owner (which is a Person).  The attributes property might
	 * look like:
	 *
	 *     attributes : {
	 *       tasks : 'App.Models.Task.models'
	 *       owner: 'App.Models.Person.model'
	 *     }
	 *
	 * This points tasks and owner properties to use
	 * <code>Task.models</code> and <code>Person.model</code>
	 * to convert the raw data into an array of Tasks and a Person.
	 *
	 * Note that the full names of the models themselves are <code>App.Models.Task</code>
	 * and <code>App.Models.Person</code>. The _.model_ and _.models_ parts are appended
	 * for the benefit of [$.Model.convert convert] to identify the types as
	 * models.
	 *
	 * @demo jquery/model/pages/associations.html
	 *
	 */
	attributes: {},
	/**
	 * $.Model.model is used as a [http://api.jquery.com/extending-ajax/#Converters Ajax converter]
	 * to convert the response of a [$.Model.findOne] request
	 * into a model instance.
	 *
	 * You will never call this method directly.  Instead, you tell $.ajax about it in findOne:
	 *
	 *     $.Model('Recipe',{
	 *       findOne : function(params, success, error ){
	 *         return $.ajax({
	 *           url: '/services/recipes/'+params.id+'.json',
	 *           type: 'get',
	 *
	 *           dataType : 'json recipe.model' //LOOK HERE!
	 *         });
	 *       }
	 *     },{})
	 *
	 * This makes the result of findOne a [http://api.jquery.com/category/deferred-object/ $.Deferred]
	 * that resolves to a model instance:
	 *
	 *     var deferredRecipe = Recipe.findOne({id: 6});
	 *
	 *     deferredRecipe.then(function(recipe){
	 *       console.log('I am '+recipes.description+'.');
	 *     })
	 *
	 * ## Non-standard Services
	 *
	 * $.jQuery.model expects data to be name-value pairs like:
	 *
	 *     {id: 1, name : 'justin'}
	 *
	 * It can also take an object with attributes in a data, attributes, or
	 * 'shortName' property.  For a App.Models.Person model the following will  all work:
	 *
	 *     { data : {id: 1, name : 'justin'} }
	 *
	 *     { attributes : {id: 1, name : 'justin'} }
	 *
	 *     { person : {id: 1, name : 'justin'} }
	 *
	 *
	 * ### Overwriting Model
	 *
	 * If your service returns data like:
	 *
	 *     {id : 1, name: 'justin', data: {foo : 'bar'} }
	 *
	 * This will confuse $.Model.model.  You will want to overwrite it to create
	 * an instance manually:
	 *
	 *     $.Model('Person',{
	 *       model : function(data){
	 *         return new this(data);
	 *       }
	 *     },{})
	 *
	 *
	 * @param {Object} attributes An object of name-value pairs or an object that has a
	 *  data, attributes, or 'shortName' property that maps to an object of name-value pairs.
	 * @return {Model} an instance of the model
	 */
	model: function( attributes ) {
		if (!attributes ) {
			return null;
		}
		if ( attributes instanceof this ) {
			attributes = attributes.serialize();
		}
		return new this(
		// checks for properties in an object (like rails 2.0 gives);
		isObject(attributes[this._shortName]) || isObject(attributes.data) || isObject(attributes.attributes) || attributes);
	},
	/**
	 * $.Model.models is used as a [http://api.jquery.com/extending-ajax/#Converters Ajax converter]
	 * to convert the response of a [$.Model.findAll] request
	 * into an array (or [$.Model.List $.Model.List]) of model instances.
	 *
	 * You will never call this method directly.  Instead, you tell $.ajax about it in findAll:
	 *
	 *     $.Model('Recipe',{
	 *       findAll : function(params, success, error ){
	 *         return $.ajax({
	 *           url: '/services/recipes.json',
	 *           type: 'get',
	 *           data: params
	 *
	 *           dataType : 'json recipe.models' //LOOK HERE!
	 *         });
	 *       }
	 *     },{})
	 *
	 * This makes the result of findAll a [http://api.jquery.com/category/deferred-object/ $.Deferred]
	 * that resolves to a list of model instances:
	 *
	 *     var deferredRecipes = Recipe.findAll({});
	 *
	 *     deferredRecipes.then(function(recipes){
	 *       console.log('I have '+recipes.length+'recipes.');
	 *     })
	 *
	 * ## Non-standard Services
	 *
	 * $.jQuery.models expects data to be an array of name-value pairs like:
	 *
	 *     [{id: 1, name : 'justin'},{id:2, name: 'brian'}, ...]
	 *
	 * It can also take an object with additional data about the array like:
	 *
	 *     {
	 *       count: 15000 //how many total items there might be
	 *       data: [{id: 1, name : 'justin'},{id:2, name: 'brian'}, ...]
	 *     }
	 *
	 * In this case, models will return an array of instances found in
	 * data, but with additional properties as expandos on the array:
	 *
	 *     var people = Person.models({
	 *       count : 1500,
	 *       data : [{id: 1, name: 'justin'}, ...]
	 *     })
	 *     people[0].name // -> justin
	 *     people.count // -> 1500
	 *
	 * ### Overwriting Models
	 *
	 * If your service returns data like:
	 *
	 *     {ballers: [{name: 'justin', id: 5}]}
	 *
	 * You will want to overwrite models to pass the base models what it expects like:
	 *
	 *     $.Model('Person',{
	 *       models : function(data){
	 *         return this._super(data.ballers);
	 *       }
	 *     },{})
	 *
	 * @param {Array} instancesRawData an array of raw name - value pairs.
	 * @return {Array} a JavaScript array of instances or a [$.Model.List list] of instances
	 *  if the model list plugin has been included.
	 */
	models: function( instancesRawData ) {
		if (!instancesRawData ) {
			return null;
		}
		// get the list type
		var res = getList(this.List),
			// did we get an array
			arr = isArray(instancesRawData),
			// cache model list
			ML = $.Model.List,
			// did we get a model list?
			ml = (ML && instancesRawData instanceof ML),
			// get the raw array of objects
			raw = arr ?
			// if an array, return the array
			instancesRawData :
			// otherwise if a model list
			(ml ?
			// get the raw objects from the list
			instancesRawData.serialize() :
			// get the object's data
			instancesRawData.data),
			// the number of items
			length = raw ? raw.length : null,
			i = 0;

		//!steal-remove-start
		if (!length ) {
			steal.dev.warn('model.js models has no data.  If you have one item, use model')
		}
		//!steal-remove-end
		for (; i < length; i++ ) {
			res.push(this.model(raw[i]));
		}
		if (!arr ) { //push other stuff onto array
			each(instancesRawData, function(prop, val){
				if ( prop !== 'data' ) {
					res[prop] = val;
				}
			})
		}
		return res;
	},
	/**
	 * The name of the id field.  Defaults to 'id'. Change this if it is something different.
	 *
	 * For example, it's common in .NET to use Id.  Your model might look like:
	 *
	 * @codestart
	 * $.Model('Friends',{
	 *   id: 'Id'
	 * },{});
	 * @codeend
	 */
	id: 'id',
	//if null, maybe treat as an array?
	/**
	 * Adds an attribute to the list of attributes for this class.
	 * @hide
	 * @param {String} property
	 * @param {String} type
	 */
	addAttr: function( property, type ) {
		var stub, attrs = this.attributes;

		stub = attrs[property] || (attrs[property] = type);
		return type;
	},
	/**
	 * @attribute convert
	 * @type Object
	 * An object of name-function pairs that are used to convert attributes.
	 * Check out [$.Model.attributes] or
	 * [jquery.model.typeconversion type conversion]
	 * for examples.
	 *
	 * Convert comes with the following types:
	 *
	 *   - date - Converts to a JS date.  Accepts integers or strings that work with Date.parse
	 *   - number - an integer or number that can be passed to parseFloat
	 *   - boolean - converts 'false' to false, and puts everything else through Boolean()
	 */
	convert: {
		'date': function( str ) {
			var type = typeof str;
			if ( type === 'string' ) {
				return isNaN(Date.parse(str)) ? null : Date.parse(str)
			} else if ( type === 'number' ) {
				return new Date(str)
			} else {
				return str
			}
		},
		'number': function( val ) {
			return parseFloat(val);
		},
		'boolean': function( val ) {
			return Boolean(val === 'false' ? 0 : val);
		},
		'default': function( val, error, type ) {
			var construct = getObject(type),
				context = window,
				realType;
			// if type has a . we need to look it up
			if ( type.indexOf('.') >= 0 ) {
				// get everything before the last .
				realType = type.substring(0, type.lastIndexOf('.'));
				// get the object before the last .
				context = getObject(realType);
			}
			return typeof construct == 'function' ? construct.call(context, val) : val;
		}
	},
	/**
	 * @attribute serialize
	 * @type Object
	 * An object of name-function pairs that are used to serialize attributes.
	 * Similar to [$.Model.convert], in that the keys of this object
	 * correspond to the types specified in [$.Model.attributes].
	 *
	 * For example, to serialize all dates to ISO format:
	 *
	 *
	 *     $.Model('Contact',{
	 *       attributes : {
	 *         birthday : 'date'
	 *       },
	 *       serialize : {
	 *         date : function(val, type){
	 *           return new Date(val).toISOString();
	 *         }
	 *       }
	 *     },{})
	 *
	 *     new Contact({ birthday: new Date('Oct 25, 1973') }).serialize()
	 *        // { 'birthday' : '1973-10-25T05:00:00.000Z' }
	 *
	 */
	serialize: {
		'default': function( val, type ) {
			return isObject(val) && val.serialize ? val.serialize() : val;
		},
		'date': function( val ) {
			return val && val.getTime()
		}
	},
	/**
	 * @function bind
	 */
	bind: bind,
	/**
	 * @function unbind
	 */
	unbind: unbind,
	_ajax: ajax
},
/**
 * @Prototype
 */
{
	/**
	 * Setup is called when a new model instance is created.
	 * It adds default attributes, then whatever attributes
	 * are passed to the class.
	 * Setup should never be called directly.
	 *
	 * @codestart
	 * $.Model('Recipe')
	 * var recipe = new Recipe({foo: 'bar'});
	 * recipe.foo //-> 'bar'
	 * recipe.attr('foo') //-> 'bar'
	 * @codeend
	 *
	 * @param {Object} attributes a hash of attributes
	 */
	setup: function( attributes ) {
		// so we know not to fire events
		this._init = true;
		this.attrs(extend({}, this.constructor.defaults, attributes));
		delete this._init;
	},
	/**
	 * Sets the attributes on this instance and calls save.
	 * The instance needs to have an id.  It will use
	 * the instance class's [$.Model.update update]
	 * method.
	 *
	 * @codestart
	 * recipe.update({name: 'chicken'}, success, error);
	 * @codeend
	 *
	 * The model will also publish a _updated_ event with [jquery.model.events Model Events].
	 *
	 * @param {Object} attrs the model's attributes
	 * @param {Function} success called if a successful update
	 * @param {Function} error called if there's an error
	 */
	update: function( attrs, success, error ) {
		this.attrs(attrs);
		return this.save(success, error); //on success, we should
	},
	/**
	 * Runs the validations on this model.  You can
	 * also pass it an array of attributes to run only those attributes.
	 * It returns nothing if there are no errors, or an object
	 * of errors by attribute.
	 *
	 * To use validations, it's suggested you use the
	 * model/validations plugin.
	 *
	 *     $.Model('Task',{
	 *       init : function(){
	 *         this.validatePresenceOf('dueDate')
	 *       }
	 *     },{});
	 *
	 *     var task = new Task(),
	 *         errors = task.errors()
	 *
	 *     errors.dueDate[0] //-> 'can't be empty'
	 *
	 * @param {Array} [attrs] an optional list of attributes to get errors for:
	 *
	 *     task.errors(['dueDate']);
	 *
	 * @return {Object} an object of attributeName : [errors] like:
	 *
	 *     task.errors() // -> {dueDate: ['cant' be empty']}
	 */
	errors: function( attrs ) {
		// convert attrs to an array
		if ( attrs ) {
			attrs = isArray(attrs) ? attrs : makeArray(arguments);
		}
		var errors = {},
			self = this,
			attr,
			// helper function that adds error messages to errors object
			// attr - the name of the attribute
			// funcs - the validation functions
			addErrors = function( attr, funcs ) {
				each(funcs, function( i, func ) {
					var res = func.call(self);
					if ( res ) {
						if (!errors[attr] ) {
							errors[attr] = [];
						}
						errors[attr].push(res);
					}

				});
			},
			validations = this.constructor.validations;

		// go through each attribute or validation and
		// add any errors
		each(attrs || validations || {}, function( attr, funcs ) {
			// if we are iterating through an array, use funcs
			// as the attr name
			if ( typeof attr == 'number' ) {
				attr = funcs;
				funcs = validations[attr];
			}
			// add errors to the
			addErrors(attr, funcs || []);
		});
		// return errors as long as we have one
		return $.isEmptyObject(errors) ? null : errors;

	},
	/**
	 * Gets or sets an attribute on the model using setters and
	 * getters if available.
	 *
	 * @codestart
	 * $.Model('Recipe')
	 * var recipe = new Recipe();
	 * recipe.attr('foo','bar')
	 * recipe.foo //-> 'bar'
	 * recipe.attr('foo') //-> 'bar'
	 * @codeend
	 *
	 * ## Setters
	 *
	 * If you add a set<i>AttributeName</i> method on your model,
	 * it will be used to set the value.  The set method is called
	 * with the value and is expected to return the converted value.
	 *
	 * @codestart
	 * $.Model('Recipe',{
	 *   setCreatedAt : function(raw){
	 *     return Date.parse(raw)
	 *   }
	 * })
	 * var recipe = new Recipe();
	 * recipe.attr('createdAt','Dec 25, 1995')
	 * recipe.createAt //-> Date
	 * @codeend
	 *
	 * ## Asynchronous Setters
	 *
	 * Sometimes, you want to perform an ajax request when
	 * you set a property.  You can do this with setters too.
	 *
	 * To do this, your setter should return undefined and
	 * call success with the converted value.  For example:
	 *
	 * @codestart
	 * $.Model('Recipe',{
	 *   setTitle : function(title, success, error){
	 *     $.post(
	 *       'recipe/update/'+this.id+'/title',
	 *       title,
	 *       function(){
	 *         success(title);
	 *       },
	 *       'json')
	 *   }
	 * })
	 *
	 * recipe.attr('title','fish')
	 * @codeend
	 *
	 * ## Events
	 *
	 * When you use attr, it can also trigger events.  This is
	 * covered in [$.Model.prototype.bind].
	 *
	 * @param {String} attribute the attribute you want to set or get
	 * @param {String|Number|Boolean} [value] value the value you want to set.
	 * @param {Function} [success] an optional success callback.
	 *    This gets called if the attribute was successful.
	 * @param {Function} [error] an optional success callback.
	 *    The error function is called with validation errors.
	 */
	attr: function( attribute, value, success, error ) {
		// get the getter name getAttrName
		var cap = classize(attribute),
			get = 'get' + cap;

		// if we are setting the property
		if ( value !== undefined ) {
			// the potential setter name
			var setName = 'set' + cap,
				//the old value
				old = this[attribute],
				self = this,
				// if an error happens, this gets called
				// it calls back the error handler
				errorCallback = function( errors ) {
					var stub;
					stub = error && error.call(self, errors);
					trigger(self, 'error.' + attribute, errors);
				};

			// if we have a setter
			if ( this[setName] &&
			// call the setter, if returned value is undefined,
			// this means the setter is async so we
			// do not call update property and return right away
			(value = this[setName](value,
			// a success handler we pass to the setter, it needs to call
			// this if it returns undefined
			this.proxy('_updateProperty', attribute, value, old, success, errorCallback), errorCallback)) === undefined ) {
				return;
			}
			// call update property which will actually update the property
			this._updateProperty(attribute, value, old, success, errorCallback);
			return this;
		}
		// get the attribute, check if we have a getter, otherwise, just get the data
		return this[get] ? this[get]() : this[attribute];
	},

	/**
	 * @function bind
	 * Binds to events on this model instance.  Typically
	 * you'll bind to an attribute name.  Handler will be called
	 * every time the attribute value changes.  For example:
	 *
	 * @codestart
	 * $.Model('School')
	 * var school = new School();
	 * school.bind('address', function(ev, address){
	 *   alert('address changed to '+address);
	 * })
	 * school.attr('address','1124 Park St');
	 * @codeend
	 *
	 * You can also bind to attribute errors.
	 *
	 * @codestart
	 * $.Model('School',{
	 *   setName : function(name, success, error){
	 *     if(!name){
	 *        error('no name');
	 *     }
	 *     return error;
	 *   }
	 * })
	 * var school = new School();
	 * school.bind('error.name', function(ev, mess){
	 *    mess // -> 'no name';
	 * })
	 * school.attr('name','');
	 * @codeend
	 *
	 * You can also bind to created, updated, and destroyed events.
	 *
	 * @param {String} eventType the name of the event.
	 * @param {Function} handler a function to call back when an event happens on this model.
	 * @return {model} the model instance for chaining
	 */
	bind: bind,
	/**
	 * @function unbind
	 * Unbinds an event handler from this instance.
	 * Read [$.Model.prototype.bind] for
	 * more information.
	 * @param {String} eventType
	 * @param {Function} handler
	 */
	unbind: unbind,
	// Actually updates a property on a model.  This
	// - Triggers events when a property has been updated
	// - uses converters to change the data type
	// propety - the attribute name
	// value - the new value
	// old - the old value
	// success -
	_updateProperty: function( property, value, old, success, errorCallback ) {
		var Class = this.constructor,
			// the value that we will set
			val,
			// the type of the attribute
			type = Class.attributes[property] || Class.addAttr(property, 'string'),
			//the converter
			converter = Class.convert[type] || Class.convert['default'],
			// errors for this property
			errors = null,
			// the event name prefix (might be error.)
			prefix = '',
			global = 'updated.',
			args, globalArgs, callback = success,
			list = Class.list;

		// set the property value
		// notice that even if there's an error
		// property values get set
		val = this[property] =
			//if the value is null
			( value === null ?
			// it should be null
			null :
			// otherwise, the converters to make it something useful
			converter.call(Class, value, function() {}, type) );

		//validate (only if not initializing, this is for performance)
		if (!this._init ) {
			errors = this.errors(property);
		}
		// args triggered on the property event name
		args = [val];
		// args triggered on the 'global' event (updated.attr)
		globalArgs = [property, val, old];

		// if there are errors, change props so we trigger error events
		if ( errors ) {
			prefix = global = 'error.';
			callback = errorCallback;
			globalArgs.splice(1, 0, errors);
			args.unshift(errors)
		}
		// as long as we changed values, trigger events
		if ( old !== val && !this._init ) {
			!errors && trigger(this, prefix + property, args);
			trigger(this,global + 'attr', globalArgs);
		}
		callback && callback.apply(this, args);

		//if this class has a global list, add / remove from the list.
		if ( property === Class.id && val !== null && list ) {
			// if we didn't have an old id, add ourselves
			if (!old ) {
				list.push(this);
			} else if ( old != val ) {
				// if our id has changed ... well this should be ok
				list.remove(old);
				list.push(this);
			}
		}

	},

	/**
	 * Removes an attribute from the list existing of attributes.
	 * Each attribute is set with [$.Model.prototype.attr attr].
	 *
	 * @codestart
	 * recipe.removeAttr('name')
	 * @codeend
	 *
	 * @param {Object} [attribute]  the attribute to remove
	 */
	removeAttr: function( attr ) {
		var old = this[attr],
			deleted = false,
			attrs = this.constructor.attributes;

		//- pop it off the object
		if ( this[attr] ) {
			delete this[attr];
		}

		//- pop it off the Class attributes collection
		if ( attrs[attr] ) {
			delete attrs[attr];
			deleted = true;
		}

		//- trigger the update
		if (!this._init && deleted && old ) {
			trigger(this,'updated.attr', [attr, null, old]);
		}
	},

	/**
	 * Gets or sets a list of attributes.
	 * Each attribute is set with [$.Model.prototype.attr attr].
	 *
	 * @codestart
	 * recipe.attrs({
	 *   name: 'ice water',
	 *   instructions : 'put water in a glass'
	 * })
	 * @codeend
	 *
	 * This can be used nicely with [jquery.model.events].
	 *
	 * @param {Object} [attributes]  if present, the list of attributes to send
	 * @return {Object} the current attributes of the model
	 */
	attrs: function( attributes ) {
		var key, constructor = this.constructor,
			attrs = constructor.attributes;
		if (!attributes ) {
			attributes = {};
			for ( key in attrs ) {
				if ( attrs.hasOwnProperty(key) ) {
					attributes[key] = this.attr(key);
				}
			}
		} else {
			var idName = constructor.id;
			//always set the id last
			for ( key in attributes ) {
				if ( key != idName ) {
					this.attr(key, attributes[key]);
				}
			}
			if ( idName in attributes ) {
				this.attr(idName, attributes[idName]);
			}

		}
		return attributes;
	},
	/**
	 * Get a serialized object for the model. Serialized data is typically
	 * used to send back to a server. See [$.Model.serialize].
	 *
	 *     model.serialize() // -> { name: 'Fred' }
	 *
	 * @return {Object} a JavaScript object that can be serialized with
	 * `JSON.stringify` or other methods.
	 */
	serialize: function() {
		var Class = this.constructor,
			attrs = Class.attributes,
			type, converter, data = {},
			attr;

		attributes = {};

		for ( attr in attrs ) {
			if ( attrs.hasOwnProperty(attr) ) {
				type = attrs[attr];
				// the attribute's converter or the default converter for the class
				converter = Class.serialize[type] || Class.serialize['default'];
				data[attr] = converter.call(Class, this[attr], type);
			}
		}
		return data;
	},
	/**
	 * Returns if the instance is a new object.  This is essentially if the
	 * id is null or undefined.
	 *
	 *     new Recipe({id: 1}).isNew() //-> false
	 * @return {Boolean} false if an id is set, true if otherwise.
	 */
	isNew: function() {
		var id = getId(this);
		return (id === undefined || id === null || id === ''); //if null or undefined
	},
	/**
	 * Creates or updates the instance using [$.Model.create] or
	 * [$.Model.update] depending if the instance
	 * [$.Model.prototype.isNew has an id or not].
	 *
	 * When a save is successful, `success` is called and depending if the
	 * instance was created or updated, a created or updated event is fired.
	 *
	 * ### Example
	 *
	 *     $.Model('Recipe',{
	 *       created : '/recipes',
	 *       updated : '/recipes/{id}.json'
	 *     },{})
	 *
	 *     // create a new instance
	 *     var recipe = new Recipe({name: 'ice water'});
	 *
	 *     // listen for when it is created or updated
	 *     recipe.bind('created', function(ev, recipe){
	 *       console.log('created', recipe.id)
	 *     }).bind('updated', function(ev, recipe){
	 *       console.log('updated', recipe.id );
	 *     })
	 *
	 *     // create the recipe on the server
	 *     recipe.save(function(){
	 *       // update the recipe's name
	 *       recipe.attr('name','Ice Water');
	 *
	 *       // update the recipe on the server
	 *       recipe.save();
	 *     }, error);
	 *
	 *
	 * @param {Function} [success] called with (instance,data) if a successful save.
	 * @param {Function} [error] error handler function called with (jqXHR) if the
	 * save was not successful. It is passed the ajax request's jQXHR object.
	 * @return {$.Deferred} a jQuery deferred that resolves to the instance, but
	 * after it has been created or updated.
	 */
	save: function( success, error ) {
		return makeRequest(this, this.isNew() ? 'create' : 'update', success, error);
	},

	/**
	 * Destroys the instance by calling
	 * [$.Model.destroy] with the id of the instance.
	 *
	 * @codestart
	 * recipe.destroy(success, error);
	 * @codeend
	 *
	 * If OpenAjax.hub is available, after a successful
	 * destroy '<i>modelName</i>.destroyed' is published
	 * with the model instance.
	 *
	 * @param {Function} [success] called if a successful destroy
	 * @param {Function} [error] called if an unsuccessful destroy
	 */
	destroy: function( success, error ) {
		return makeRequest(this, 'destroy', success, error, 'destroyed');
	},


	/**
	 * Returns a unique identifier for the model instance.  For example:
	 * @codestart
	 * new Todo({id: 5}).identity() //-> 'todo_5'
	 * @codeend
	 * Typically this is used in an element's shortName property so you can find all elements
	 * for a model with [$.Model.prototype.elements elements].
	 * @return {String}
	 */
	identity: function() {
		var id = getId(this),
			constructor = this.constructor;
		return (constructor._fullName + '_' + (constructor.escapeIdentity ? encodeURIComponent(id) : id)).replace(/ /g, '_');
	},

	/**
	 * Returns elements that represent this model instance.  For this to work, your element's should
	 * us the [$.Model.prototype.identity identity] function in their class name.  Example:
	 *
	 *     <div class='todo <%= todo.identity() %>'> ... </div>
	 *
	 * This also works if you hooked up the model:
	 *
	 *     <div <%= todo %>> ... </div>
	 *
	 * Typically, you'll use this as a response to a Model Event:
	 *
	 *     '{Todo} destroyed': function(Todo, event, todo){
	 *       todo.elements(this.element).remove();
	 *     }
	 *
	 *
	 * @param {String|jQuery|element} context If provided, only elements inside this element
	 * that represent this model will be returned.
	 *
	 * @return {jQuery} Returns a jQuery wrapped nodelist of elements that have this model instances
	 *  identity in their class name.
	 */
	elements: function( context ) {
		var id = this.identity();
		if( this.constructor.escapeIdentity ) {
			id = id.replace(/([ #;&,.+*~\'%:'!^$[\]()=>|\/])/g,'\\$1')
		}

		return $('.' + id, context);
	},

	hookup: function( el ) {
		var shortName = this.constructor._shortName,
			models = $.data(el, 'models') || $.data(el, 'models', {});
		$(el).addClass(shortName + ' ' + this.identity());
		models[shortName] = this;
	}
});


each([
/**
 * @function created
 * @hide
 * Called by save after a new instance is created.  Publishes 'created'.
 * @param {Object} attrs
 */
'created',
/**
 * @function updated
 * @hide
 * Called by save after an instance is updated.  Publishes 'updated'.
 * @param {Object} attrs
 */
'updated',
/**
 * @function destroyed
 * @hide
 * Called after an instance is destroyed.
 *   - Publishes 'shortName.destroyed'.
 *   - Triggers a 'destroyed' event on this model.
 *   - Removes the model from the global list if its used.
 *
 */
'destroyed'], function( i, funcName ) {
	$.Model.prototype[funcName] = function( attrs ) {
		var stub, constructor = this.constructor;

		// remove from the list if instance is destroyed
		if ( funcName === 'destroyed' && constructor.list ) {
			constructor.list.remove(getId(this));
		}

		// update attributes if attributes have been passed
		stub = attrs && typeof attrs == 'object' && this.attrs(attrs.attrs ? attrs.attrs() : attrs);

		// call event on the instance
		trigger(this,funcName);

		//!steal-remove-start
		steal.dev.log('Model.js - '+ constructor.shortName+' '+ funcName);
		//!steal-remove-end

		// call event on the instance's Class
		trigger(constructor,funcName, this);
		return [this].concat(makeArray(arguments)); // return like this for this.proxy chains
	};
});

/**
 *  @add jQuery.fn
 */
// break
/**
 * @function models
 * Returns a list of models.  If the models are of the same
 * type, and have a [$.Model.List], it will return
 * the models wrapped with the list.
 *
 * @codestart
 * $('.recipes').models() //-> [recipe, ...]
 * @codeend
 *
 * @param {jQuery.Class} [type] if present only returns models of the provided type.
 * @return {Array|$.Model.List} returns an array of model instances that are represented by the contained elements.
 */
$.fn.models = function( type ) {
	//get it from the data
	var collection = [],
		kind, ret, retType;
	this.each(function() {
		each($.data(this, 'models') || {}, function( name, instance ) {
			//either null or the list type shared by all classes
			kind = kind === undefined ? instance.constructor.List || null : (instance.constructor.List === kind ? kind : null);
			collection.push(instance);
		});
	});

	ret = getList(kind);

	ret.push.apply(ret, unique(collection));
	return ret;
};
/**
 * @function model
 *
 * Returns the first model instance found from [jQuery.fn.models] or
 * sets the model instance on an element.
 *
 *     //gets an instance
 *     '.edit click' : function(el) {
 *       el.closest('.todo').model().destroy()
 *     },
 *     // sets an instance
 *     list : function(items){
 *        var el = this.element;
 *        $.each(item, function(item){
 *          $('<div/>').model(item)
 *            .appendTo(el)
 *        })
 *     }
 *
 * @param {Object} [type] The type of model to return.  If a model instance is provided
 * it will add the model to the element.
 */
$.fn.model = function( type ) {
	if ( type && type instanceof $.Model ) {
		type.hookup(this[0]);
		return this;
	} else {
		return this.models.apply(this, arguments)[0];
	}
};

module.exports = baseModel;
},{"./baseClass.js":2,"./helpers.js":5}],5:[function(require,module,exports){
// Several of the methods in this plugin use code adapated from Prototype
//  Prototype JavaScript framework, version 1.6.0.1
//  (c) 2005-2007 Sam Stephenson
var regs = {
    undHash: /_|-/,
    colons: /::/,
    words: /([A-Z]+)([A-Z][a-z])/g,
    lowUp: /([a-z\d])([A-Z])/g,
    dash: /([a-z\d])([A-Z])/g,
    replacer: /\{([^\}]+)\}/g,
    dot: /\./
},
// gets the nextPart property from current
// add - if true and nextPart doesnt exist, create it as an empty object
getNext = function(current, nextPart, add){
    return current[nextPart] !== undefined ? current[nextPart] : ( add && (current[nextPart] = {}) );
},
// returns true if the object can have properties (no nulls)
isContainer = function(current){
    var type = typeof current;
    return current && ( type == 'function' || type == 'object' );
},
// a reference
getObject,

/**
 * @class jQuery.String
 */
str = $.String = $.extend($.String || {} , {


    /**
     * @function getObject
     * Gets an object from a string.  It can also modify objects on the
     * 'object path' by removing or adding properties.
     *
     *     Foo = {Bar: {Zar: {"Ted"}}}
     *     $.String.getObject("Foo.Bar.Zar") //-> "Ted"
     *
     * @param {String} name the name of the object to look for
     * @param {Array} [roots] an array of root objects to look for the
     *   name.  If roots is not provided, the window is used.
     * @param {Boolean} [add] true to add missing objects to
     *  the path. false to remove found properties. undefined to
     *  not modify the root object
     * @return {Object} The object.
     */
    getObject : getObject = function( name, roots, add ) {

        // the parts of the name we are looking up
        // ['App','Models','Recipe']
        var parts = name ? name.split(regs.dot) : [],
            length =  parts.length,
            current,
            ret,
            i,
            r = 0,
            type;

        // make sure roots is an array
        roots = $.isArray(roots) ? roots : [roots || window];

        if(length == 0){
            return roots[0];
        }
        // for each root, mark it as current
        while( current = roots[r++] ) {
            // walk current to the 2nd to last object
            // or until there is not a container
            for (i =0; i < length - 1 && isContainer(current); i++ ) {
                current = getNext(current, parts[i], add);
            }
            // if we can get a property from the 2nd to last object
            if( isContainer(current) ) {

                // get (and possibly set) the property
                ret = getNext(current, parts[i], add);

                // if there is a value, we exit
                if( ret !== undefined ) {
                    // if add is false, delete the property
                    if ( add === false ) {
                        delete current[parts[i]];
                    }
                    return ret;

                }
            }
        }
    },
    /**
     * Capitalizes a string
     * @param {String} s the string.
     * @return {String} a string with the first character capitalized.
     */
    capitalize: function( s, cache ) {
        return s.charAt(0).toUpperCase() + s.substr(1);
    },
    /**
     * Capitalizes a string from something undercored. Examples:
     * @codestart
     * jQuery.String.camelize("one_two") //-> "oneTwo"
     * "three-four".camelize() //-> threeFour
     * @codeend
     * @param {String} s
     * @return {String} a the camelized string
     */
    camelize: function( s ) {
        s = str.classize(s);
        return s.charAt(0).toLowerCase() + s.substr(1);
    },
    /**
     * Like [jQuery.String.camelize|camelize], but the first part is also capitalized
     * @param {String} s
     * @return {String} the classized string
     */
    classize: function( s , join) {
        var parts = s.split(regs.undHash),
            i = 0;
        for (; i < parts.length; i++ ) {
            parts[i] = str.capitalize(parts[i]);
        }

        return parts.join(join || '');
    },
    /**
     * Like [jQuery.String.classize|classize], but a space separates each 'word'
     * @codestart
     * jQuery.String.niceName("one_two") //-> "One Two"
     * @codeend
     * @param {String} s
     * @return {String} the niceName
     */
    niceName: function( s ) {
        return str.classize(s,' ');
    },

    /**
     * Underscores a string.
     * @codestart
     * jQuery.String.underscore("OneTwo") //-> "one_two"
     * @codeend
     * @param {String} s
     * @return {String} the underscored string
     */
    underscore: function( s ) {
        return s.replace(regs.colons, '/').replace(regs.words, '$1_$2').replace(regs.lowUp, '$1_$2').replace(regs.dash, '_').toLowerCase();
    },
    /**
     * Returns a string with {param} replaced values from data.
     *
     *     $.String.sub("foo {bar}",{bar: "far"})
     *     //-> "foo far"
     *
     * @param {String} s The string to replace
     * @param {Object} data The data to be used to look for properties.  If it's an array, multiple
     * objects can be used.
     * @param {Boolean} [remove] if a match is found, remove the property from the object
     */
    sub: function( s, data, remove ) {
        var obs = [],
            remove = typeof remove == 'boolean' ? !remove : remove;
        obs.push(s.replace(regs.replacer, function( whole, inside ) {
            //convert inside to type
            var ob = getObject(inside, data, remove);

            // if a container, push into objs (which will return objects found)
            if( isContainer(ob) ){
                obs.push(ob);
                return "";
            }else{
                return ""+ob;
            }
        }));

        return obs.length <= 1 ? obs[0] : obs;
    },
    _regs : regs
});
},{}],6:[function(require,module,exports){
var Class = require('common/baseClass.js');

Class('EventEmitter', {
    subscribe : PubSub.subscribe,
    publish : PubSub.publish,
    unsubscribe : PubSub.unsubscribe
}, {});

module.exports = EventEmitter;
},{"common/baseClass.js":2}],7:[function(require,module,exports){
var Model = require('common/baseModel.js');

Model('Mec.Models.Product', {}, {});
},{"common/baseModel.js":4}],8:[function(require,module,exports){
var $Controller = require('common/baseController.js');
require('services/product.js');

module.exports = $Controller('Mec.Searcher.Products',
/* @Static */
{
	defaults : {
		searchEvent : 'searcher.search'
	}
},
/* @Prototype */
{
	init : function() {
		var self = this;
		var options = self.options;

		// listen for search event
		EventEmitter.subscribe(options.searchEvent, this.callback('searchSubscriber'));
	},

	/**
	 * Requests products for provided query
	 */
	searchSubscriber : function(ev, searchQuery) {
		//console.log(searchQuery);

		var productsList = Mec.Services.Product.findAll({
			keywords : searchQuery
		});

		$.when(productsList).done(this.callback('render'));
	},

	/**
	 * Render products into view
	 */
	render : function(data) {
		console.log(data.products[0]);
		//this.scope.find('p').html('Searching for ' + searchQuery);
	}
});

// create a jquery plugin
$.fn.mec_searcher_products = function(options) {
	options = (options ? options : {});

	return new Mec.Searcher.Products(this, options);
}
},{"common/baseController.js":3,"services/product.js":11}],9:[function(require,module,exports){
var Controller = require('common/baseController.js');
var EventEmitter = require('models/event.js');

module.exports = Controller('Mec.Searcher.Search',
/* @Static */
{
	defaults : {
		selectors : {
			searchField : '#search-field',
			searchTrigger : '#search-trigger'
		},
		query : '',
		searchEvent : 'searcher.search'
	}
},
/* @Prototype */
{
	init : function() {
		var self = this;
		var options = self.options;
		var selectors = self.options.selectors;

		// bind to events
		self.scope.find(selectors.searchField).on('keyup', function(ev) {
			self.handleFieldKeyup($(this), ev);
		});
		self.scope.find(selectors.searchTrigger).on('click', function(ev) {
			self.handleTriggerClick($(this), ev);
		});
	},

	handleFieldKeyup : function(el, ev) {
		if(ev.which == '13') {
			var query = el.val();

			this.search(query);
		}
	},

	handleTriggerClick : function(el, ev) {
		var searchFieldEl = this.scope.find(this.options.selectors.searchField);
		var query = searchFieldEl.val();

		this.search(query);
	},

	search : function(newQuery) {
		var currentQuery = this.options.query;

		if(newQuery !== currentQuery && newQuery !== '') {
			EventEmitter.publish(this.options.searchEvent, newQuery);
		}
	}
});

// create a jquery plugin
$.fn.mec_searcher_search = function(options) {
	options = (options ? options : {});

	return new Mec.Searcher.Search(this, options);
}
},{"common/baseController.js":3,"models/event.js":6}],10:[function(require,module,exports){
require('searcher/products/products.js');
require('searcher/search/search.js');

$(document).ready(function() {
    // init search
    $('#searcher').mec_searcher_search();

    // init products
    $('#products-container').mec_searcher_products();
});
},{"searcher/products/products.js":8,"searcher/search/search.js":9}],11:[function(require,module,exports){
var Class = require('common/baseClass.js');
require('models/product.js');

Class('Mec.Services.Product',
/* @Static */
{
    defaults : {
        domain : 'http://www.mec.ca',
        apiPath : '/api/v1',
        url : '/products/search'
    },

    /**
     * Products lookup
     */
    findAll : function(params, success, error) {
        var self = this;

        return this.resource(params, success, error);
    },

    /**
     * Make an ajax request and return a promise
     */
    resource : function(params, success, error) {
        var options = this.defaults;
        var url = options.domain + options.apiPath + options.url;

        params = $.extend(true, {
            keywords : ''
        }, params);

        return $.ajax(url, {
            data : params
        }).then(this.hydrate);
    },

    /**
     * Hydrate json with model instances
     */
    hydrate : function(data) {
        if(data && data.products) {
            var products = [];

            for(var i = 0, len = data.products.length; i < len; i ++) {
                products.push(new Mec.Models.Product(data.products[i]));
            }

            data.products = products;
        }

        return data;
    }
},
/* @Prototype */
{});
},{"common/baseClass.js":2,"models/product.js":7}]},{},[1]);
