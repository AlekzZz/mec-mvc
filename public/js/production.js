(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

// load global event model
window.Event = require('lib/event.js');

// load searcher module
require('searcher/searcher.js');
},{"lib/event.js":6,"searcher/searcher.js":11}],2:[function(require,module,exports){
(function() {
    var StringHelper = require('./helpers.js');

    // baseClass
    // This is a modified version of John Resig's class
    // http://ejohn.org/blog/simple-javascript-inheritance/
    // It provides class level inheritance and callbacks.
    //
    // =============== HELPERS =================
    // if we are initializing a new class
    var initializing = false;
    var makeArray = $.makeArray;
    var isFunction = $.isFunction;
    var isArray = $.isArray;
    var extend = $.extend;
    var getObject = StringHelper.getObject;

    var concatArgs = function(arr, args) {
        return arr.concat(makeArray(args));
    };

    // tests if we can get super in .toString()
    var fnTest = /xyz/.test(function() { xyz; }) ? /\b_super\b/ : /.*/;

    // overwrites an object with methods, sets up _super
    //   newProps - new properties
    //   oldProps - where the old properties might be
    //   addTo - what we are adding to
    var inheritProps = function(newProps, oldProps, addTo) {
        addTo = addTo || newProps;

        for (var name in newProps) {
            // Check if we're overwriting an existing function
            addTo[name] = isFunction(newProps[name]) &&
                          isFunction(oldProps[name]) &&
                          fnTest.test(newProps[name]) ? (function(name, fn) {
                return function() {
                    var tmp = this._super,
                        ret;

                    // Add a new ._super() method that is the same method
                    // but on the super-class
                    this._super = oldProps[name];

                    // The method only need to be bound temporarily, so we
                    // remove it when we're done executing
                    ret = fn.apply(this, arguments);
                    this._super = tmp;
                    return ret;
                };
            })(name, newProps[name]) : newProps[name];
        }
    };

    var STR_PROTOTYPE = 'prototype';

    /**
     * @class baseClass
     */
    var baseClass = function() {
        if (arguments.length) {
            return baseClass.extend.apply(baseClass, arguments);
        }
    };

    /* @Static*/
    extend(baseClass, {
        proxy: function(funcs) {

            //args that should be curried
            var args = makeArray(arguments),
                self;

            // get the functions to callback
            funcs = args.shift();

            // if there is only one function, make funcs into an array
            if (!isArray(funcs)) {
                funcs = [funcs];
            }

            // keep a reference to us in self
            self = this;

            for (var i = 0; i < funcs.length; i++) {
                if (typeof funcs[i] === 'string' && !isFunction(this[funcs[i]])) {
                    throw ('class.js ' + (this.fullName || this.Class.fullName) + ' does not have a ' + funcs[i] + 'method!');
                }
            }

            return function class_cb() {
                // add the arguments after the curried args
                var cur = concatArgs(args, arguments),
                    isString,
                    length = funcs.length,
                    f = 0,
                    func;

                // go through each function to call back
                for (; f < length; f++) {
                    func = funcs[f];
                    if (!func) {
                        continue;
                    }

                    // set called with the name of the function on self (this is how this.view works)
                    isString = typeof func === 'string';
                    if (isString && self._set_called) {
                        self.called = func;
                    }

                    // call the function
                    cur = (isString ? self[func] : func).apply(self, cur || []);

                    // pass the result to the next function (if there is a next function)
                    if (f < length - 1) {
                        cur = !isArray(cur) || cur._use_call ? [cur] : cur;
                    }
                }

                return cur;
            };
        },

        newInstance: function() {
            // get a raw instance objet (init is not called)
            var inst = this.rawInstance(),
                args;

            // call setup if there is a setup
            if (inst.setup) {
                args = inst.setup.apply(inst, arguments);
            }
            // call init if there is an init, if setup returned args, use those as the arguments
            if (inst.init) {
                inst.init.apply(inst, isArray(args) ? args : arguments);
            }

            return inst;
        },

        setup: function(baseClass, fullName) {
            // set defaults as the merger of the parent defaults and this object's defaults
            this.defaults = extend(true, {}, baseClass.defaults, this.defaults);
            return arguments;
        },

        rawInstance: function() {
            // prevent running init
            initializing = true;
            var inst = new this();
            initializing = false;
            // allow running init
            return inst;
        },

        extend: function(fullName, klass, proto) {
            // figure out what was passed and normalize it
            if (typeof fullName !== 'string') {
                proto = klass;
                klass = fullName;
                fullName = null;
            }

            if (!proto) {
                proto = klass;
                klass = null;
            }

            proto = proto || {};
            var _super_class = this,
                _super = this[STR_PROTOTYPE],
                name, shortName, namespace, prototype;

            // Instantiate a base class (but only create the instance,
            // don't run the init constructor)
            initializing = true;
            prototype = new this();
            initializing = false;

            // Copy the properties over onto the new prototype
            inheritProps(proto, _super, prototype);

            // The dummy class constructor
            function Class() {
                // All construction is actually done in the init method
                if (initializing) {
                    return undefined;
                }

                // we are being called w/o new, we are extending
                if (this.constructor !== Class && arguments.length) {
                    //return Class.extend.apply(Class, arguments);
                    return arguments.callee.extend.apply(arguments.callee, arguments);
                } else { //we are being called w/ new
                    return this.Class.newInstance.apply(this.Class, arguments);
                }
            }

            // Copy old stuff onto class
            for (name in this) {
                if (this.hasOwnProperty(name)) {
                    Class[name] = this[name];
                }
            }

            // copy new static props on class
            inheritProps(klass, this, Class);

            // do namespace stuff
            if (fullName) {
                var parts = fullName.split(/\./),
                    shortName = parts.pop(),
                    current = getObject(parts.join('.'), window, true),
                    namespace = current;

                if (current[shortName]) {
                    throw ('baseClass.js There\'s already something called ' + fullName);
                }

                //!steal-remove-end
                current[shortName] = Class;
            }

            // set things that can't be overwritten
            extend(Class, {
                prototype: prototype,
                namespace: namespace,
                shortName: shortName,
                constructor: Class,
                fullName: fullName
            });

            //make sure our prototype looks nice
            Class[STR_PROTOTYPE].Class = Class[STR_PROTOTYPE].constructor = Class;

            // call the class setup
            var args = Class.setup.apply(Class, concatArgs([_super_class], arguments));

            // call the class init
            if (Class.init) {
                Class.init.apply(Class, args || concatArgs([_super_class], arguments));
            }

            /* @Prototype*/
            return Class;
        }
    });

    // proxy "this" context to the callback
    baseClass.callback = baseClass[STR_PROTOTYPE].callback = baseClass[STR_PROTOTYPE].proxy = baseClass.proxy;

    // export base class
    module.exports = baseClass;
})();
},{"./helpers.js":7}],3:[function(require,module,exports){
'use strict';

(function() {
    var Class = require('./baseClass.js');

    /**
     * @class Controller
     * @inherits Base Class
     */
    var Controller = Class(
    /* @static */
    {},
    /* @prototype */
    {
        setup: function(options) {
            this.options = $.extend($.extend(true, {}, this.Class.defaults), options);

            return [this.scope, this.options].concat($.makeArray(arguments).slice(2));
        }
    });

    module.exports = Controller;
})();
},{"./baseClass.js":2}],4:[function(require,module,exports){
'use strict';

(function() {
    var Class = require('./baseClass.js');

    var Model = Class(
    /* @static */
    {
        defaults: {
            contract: null
        }
    },
    /* @prototype */
    {
        setup: function(attributes, options) {
            options = $.extend(true, this.defaults, options);

            // populate itself with attributes
            var prop;
            for (prop in attributes) {
                if (attributes.hasOwnProperty(prop)) {
                    if (!options.contract || (options.contract && options.contract.indexOf(prop) !== -1)) {
                        this[prop] = attributes[prop];
                    }
                }
            }
        }
    });

    module.exports = Model;
})();
},{"./baseClass.js":2}],5:[function(require,module,exports){
'use strict';

(function() {
    var baseView = function(viewName) {
        this.viewName = viewName;
    };
    baseView.prototype = {
        viewName: '',

        render: function(data) {
            return Mec.Template[this.viewName](data);
        }
    };

    module.exports = baseView;
})();
},{}],6:[function(require,module,exports){
'use strict';

(function() {
    module.exports = {
        subscribe: PubSub.subscribe,
        publish: PubSub.publish,
        unsubscribe: PubSub.unsubscribe
    };
})();
},{}],7:[function(require,module,exports){
'use strict';

(function() {
    // Several of the methods in this plugin use code adapated from Prototype
    //   Prototype JavaScript framework, version 1.6.0.1
    //   (c) 2005-2007 Sam Stephenson
    var regs = {
        undHash: /_|-/,
        colons: /::/,
        words: /([A-Z]+)([A-Z][a-z])/g,
        lowUp: /([a-z\d])([A-Z])/g,
        dash: /([a-z\d])([A-Z])/g,
        replacer: /\{([^\}]+)\}/g,
        dot: /\./
    },

    // gets the nextPart property from current
    // add - if true and nextPart doesnt exist, create it as an empty object
    getNext = function(current, nextPart, add) {
        return current[nextPart] !== undefined ? current[nextPart] : (add && (current[nextPart] = {}));
    },

    // returns true if the object can have properties (no nulls)
    isContainer = function(current) {
        var type = typeof current;

        return current && (type === 'function' || type === 'object');
    },

    /**
     * StringHelper
     */
    StringHelper = {
        /**
         * @function getObject
         * Gets an object from a string.  It can also modify objects on the
         * 'object path' by removing or adding properties.
         *
         *     Foo = {Bar: {Zar: {'Ted'}}}
         *     $.String.getObject('Foo.Bar.Zar') //-> 'Ted'
         *
         * @param {String} name the name of the object to look for
         * @param {Array} [roots] an array of root objects to look for the
         *   name.  If roots is not provided, the window is used.
         * @param {Boolean} [add] true to add missing objects to
         *  the path. false to remove found properties. undefined to
         *  not modify the root object
         * @return {Object} The object.
         */
        getObject: function(name, roots, add) {
            // the parts of the name we are looking up
            // ['App','Models','Recipe']
            var parts = name ? name.split(regs.dot) : [],
                length = parts.length,
                current,
                ret,
                i,
                r = 0;

            // make sure roots is an array
            roots = $.isArray(roots) ? roots : [roots || window];

            if (length === 0) {
                return roots[0];
            }

            // for each root, mark it as current
            while (current = roots[r++]) {
                // walk current to the 2nd to last object
                // or until there is not a container
                for (i = 0; i < length - 1 && isContainer(current); i++) {
                    current = getNext(current, parts[i], add);
                }
                // if we can get a property from the 2nd to last object
                if (isContainer(current)) {

                    // get (and possibly set) the property
                    ret = getNext(current, parts[i], add);

                    // if there is a value, we exit
                    if (ret !== undefined) {
                        // if add is false, delete the property
                        if (add === false) {
                            delete current[parts[i]];
                        }

                        return ret;

                    }
                }
            }
        },

        /**
         * Capitalizes a string
         * @param {String} s the string.
         * @return {String} a string with the first character capitalized.
         */
        /*capitalize: function( s, cache ) {
            return s.charAt(0).toUpperCase() + s.substr(1);
        },*/

        /**
         * Capitalizes a string from something undercored. Examples:
         * @codestart
         * jQuery.String.camelize('one_two') //-> 'oneTwo'
         * 'three-four'.camelize() //-> threeFour
         * @codeend
         * @param {String} s
         * @return {String} a the camelized string
         */
        /*camelize: function(s) {
            s = StringHelper.classize(s);
            return s.charAt(0).toLowerCase() + s.substr(1);
        },*/

        /**
         * Like [jQuery.String.camelize|camelize], but the first part is also capitalized
         * @param {String} s
         * @return {String} the classized string
         */
        /*classize : function(s , join) {
            var parts = s.split(regs.undHash),
                i = 0;
            for (; i < parts.length; i++ ) {
                parts[i] = StringHelper.capitalize(parts[i]);
            }

            return parts.join(join || '');
        },*/

        /**
         * Like [jQuery.String.classize|classize], but a space separates each 'word'
         * @codestart
         * jQuery.String.niceName('one_two') //-> 'One Two'
         * @codeend
         * @param {String} s
         * @return {String} the niceName
         */
        /*niceName : function( s ) {
            return StringHelper.classize(s,' ');
        },*/

        /**
         * Underscores a string.
         * @codestart
         * jQuery.String.underscore('OneTwo') //-> 'one_two'
         * @codeend
         * @param {String} s
         * @return {String} the underscored string
         */
        underscore: function(s) {
            return s.replace(regs.colons, '/').replace(regs.words, '$1_$2').replace(regs.lowUp, '$1_$2').replace(regs.dash, '_').toLowerCase();
        },

        /**
         * Returns a string with {param} replaced values from data.
         *
         *     $.String.sub('foo {bar}',{bar: 'far'})
         *     //-> 'foo far'
         *
         * @param {String} s The string to replace
         * @param {Object} data The data to be used to look for properties.  If it's an array, multiple
         * objects can be used.
         * @param {Boolean} [remove] if a match is found, remove the property from the object
         */
        /*sub: function(s, data, remove) {
            var obs = [],
                remove = typeof remove == 'boolean' ? !remove : remove;
            obs.push(s.replace(regs.replacer, function(whole, inside) {
                //convert inside to type
                var ob = StringHelper.getObject(inside, data, remove);

                // if a container, push into objs (which will return objects found)
                if(isContainer(ob)) {
                    obs.push(ob);
                    return '';
                } else {
                    return '' + ob;
                }
            }));

            return obs.length <= 1 ? obs[0] : obs;
        },*/

        _regs: regs
    };

    module.exports = StringHelper;
})();
},{}],8:[function(require,module,exports){
'use strict';

(function() {
    var Model = require('lib/baseModel.js');

    Model('Mec.Models.Product', {}, {});
})();
},{"lib/baseModel.js":4}],9:[function(require,module,exports){
'use strict';

var Controller = require('lib/baseController.js');
var View = require('lib/baseView.js');

require('services/product.js');

/**
 * @class Mec.Searcher.Products
 * @inherits Controller
 */
module.exports = Controller('Mec.Searcher.Products',
/* @Static */
{
    defaults: {
        searchEvent: 'searcher.search',
        view: new View('searcherProducts')
    }
},
/* @Prototype */
{
    init: function() {
        var self = this;
        var options = self.options;

        // listen for search event
        Event.subscribe(options.searchEvent, this.callback('searchSubscriber'));
    },

    /**
     * Requests products for provided query
     */
    searchSubscriber: function(ev, searchQuery) {
        var productsList = Mec.Services.Product.findAll({
            keywords: searchQuery
        });

        productsList.then(this.callback('render'));
    },

    /**
     * Render products into view
     */
    render: function(data) {
        var self = this;
        var view = self.options.view;

        this.scope.html(view.render(data));
    }
});

// create a jquery plugin
$.fn.mec_searcher_products = function(options) {
    options = (options ? options : {});

    return new Mec.Searcher.Products(this, options);
};
},{"lib/baseController.js":3,"lib/baseView.js":5,"services/product.js":13}],10:[function(require,module,exports){
'use strict';

var Controller = require('lib/baseController.js');

Controller('Mec.Searcher.Search',
/* @Static */
{
    defaults: {
        selectors: {
            searchField: '#search-field',
            searchTrigger: '#search-trigger'
        },
        query: '',
        searchEvent: 'searcher.search'
    }
},
/* @Prototype */
{
    init: function() {
        var self = this;
        var selectors = self.options.selectors;

        // bind to events
        self.scope.find(selectors.searchField).on('keyup', function(ev) {
            self.handleFieldKeyup($(this), ev);
        });
        self.scope.find(selectors.searchTrigger).on('click', function(ev) {
            self.handleTriggerClick($(this), ev);
        });
    },

    handleFieldKeyup: function(el, ev) {
        if (ev.which === 13) {
            var query = el.val();

            this.search(query);
        }
    },

    handleTriggerClick: function(el, ev) {
        var searchFieldEl = this.scope.find(this.options.selectors.searchField);
        var query = searchFieldEl.val();

        this.search(query);
    },

    search: function(newQuery) {
        var currentQuery = this.options.query;

        if (newQuery !== currentQuery && newQuery !== '') {
            this.options.query = newQuery;

            // trigger search event
            Event.publish(this.options.searchEvent, newQuery);
        }
    }
});

// create a jquery plugin
$.fn.mec_searcher_search = function(options) {
    options = (options ? options : {});

    return new Mec.Searcher.Search(this, options);
};
},{"lib/baseController.js":3}],11:[function(require,module,exports){
'use strict';

require('searcher/products/products.js');
require('searcher/search/search.js');
require('searcher/tabs/tabs.js');

$(document).ready(function() {
    // init search
    $('#searcher').mec_searcher_search();

    // init products
    $('#products-container').mec_searcher_products();

    // init tabs (no plugin)
    new Mec.Searcher.Tabs('#searcher-tabs');
});
},{"searcher/products/products.js":9,"searcher/search/search.js":10,"searcher/tabs/tabs.js":12}],12:[function(require,module,exports){
/* globals Ui */
'use strict';

(function() {
    require('ui/tabs/tabs.js');

    /**
     * @class Mec.Searcher.Tabs
     * @inherits Ui.Tabs
     */
    Ui.Tabs('Mec.Searcher.Tabs', {}, {});
})();
},{"ui/tabs/tabs.js":14}],13:[function(require,module,exports){
'use strict';

(function() {
    var Class = require('lib/baseClass.js');
    require('models/product.js');

    Class('Mec.Services.Product',
    /* @Static */
    {
        defaults: {
            domain: 'http://www.mec.ca',
            apiPath: '/api/v1',
            url: '/products/search',
            model: Mec.Models.Product,
            contract: null
        },

        /**
         * Products lookup
         */
        findAll: function(params, success, error) {
            return this.resource(params, success, error);
        },

        /**
         * Make an ajax request and return a promise
         */
        resource: function(params, success, error) {
            var options = this.defaults;
            var url = options.domain + options.apiPath + options.url;

            // set contract for the resource
            this.contract = (params.contract ? params.contract : null);

            // merge params
            params = $.extend(true, {
                keywords: ''
            }, params);

            return $.ajax(url, {
                data: params
            }).then(this.callback('hydrate'));
        },

        /**
         * Hydrate json with model instances
         */
        hydrate: function(data) {
            var options = this.defaults;

            if (data && data.products) {
                var products = [];

                for (var i = 0, len = data.products.length; i < len; i++) {
                    var product = new options.model(data.products[i], {
                        contract: options.contract
                    });

                    products.push(product);
                }

                data.products = products;
            }

            return data;
        }
    },
    /* @Prototype */
    {});
})();
},{"lib/baseClass.js":2,"models/product.js":8}],14:[function(require,module,exports){
'use strict';

(function() {
    var Controller = require('lib/baseController.js');

    /**
     * @class Ui.Tabs
     * @inherits Controller
     */
    Controller('Ui.Tabs',
    /** @Static */
    {
        defaults: {
            selectors: {
                tab: '.ui-tab',
                content: '.ui-tab-content'
            },
            activeClass: 'active',
            onInit: function() {},
            onTabClick: function() {},
            onContentShow: function() {}
        }
    },
    /** @Prototype */
    {
        /**
         * Main Init
         */
        init: function() {
            var self = this;
            var options = self.options;
            var tab = self.scope.find(options.selectors.tab);
            var content = self.scope.find(options.selectors.content);

            // Trigger initial tab
            if (tab.filter('.' + options.activeClass).length === 0) {
                tab.filter(':first').addClass(options.activeClass).siblings().removeClass(options.activeClass);
                content.filter(':first').addClass(options.activeClass).siblings().removeClass(options.activeClass);
            } else {
                var index = tab.filter('.' + options.activeClass).index();
                content.eq(index).addClass(options.activeClass).siblings().removeClass(options.activeClass);
            }

            // Call onInit callback
            options.onInit(self.scope, options);

            // event listeners
            self.scope.on('click', options.selectors.tab, function(ev) {
                self.tabClickHandler($(this), ev);
            });
        },

        /**
         * Tab click handler
         */
        tabClickHandler: function(el, ev) {
            ev.preventDefault();

            var self = this;
            var options = self.options;

            // Call onTabClick callback
            options.onTabClick(el, ev);

            // Set active class for selected tab based on index
            el.addClass(options.activeClass).siblings().removeClass(options.activeClass);

            // Set active class for selected content based on tab index
            var content = self.scope.find(options.selectors.content).eq(el.index());
            self.contentShow(content);
        },

        /**
         * Content display
         */
        contentShow: function(el) {
            var self = this;
            var options = self.options;

            // Set active class for selected tab based on index
            el.addClass(options.activeClass).siblings().removeClass(options.activeClass);

            // Call onContentShow callback
            options.onContentShow(el);
        }
    });
})();
},{"lib/baseController.js":3}]},{},[1])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvYXBwL2FwcC5qcyIsInNyYy9qcy9hcHAvbGliL2Jhc2VDbGFzcy5qcyIsInNyYy9qcy9hcHAvbGliL2Jhc2VDb250cm9sbGVyLmpzIiwic3JjL2pzL2FwcC9saWIvYmFzZU1vZGVsLmpzIiwic3JjL2pzL2FwcC9saWIvYmFzZVZpZXcuanMiLCJzcmMvanMvYXBwL2xpYi9ldmVudC5qcyIsInNyYy9qcy9hcHAvbGliL2hlbHBlcnMuanMiLCJzcmMvanMvYXBwL21vZGVscy9wcm9kdWN0LmpzIiwic3JjL2pzL2FwcC9zZWFyY2hlci9wcm9kdWN0cy9wcm9kdWN0cy5qcyIsInNyYy9qcy9hcHAvc2VhcmNoZXIvc2VhcmNoL3NlYXJjaC5qcyIsInNyYy9qcy9hcHAvc2VhcmNoZXIvc2VhcmNoZXIuanMiLCJzcmMvanMvYXBwL3NlYXJjaGVyL3RhYnMvdGFicy5qcyIsInNyYy9qcy9hcHAvc2VydmljZXMvcHJvZHVjdC5qcyIsInNyYy9qcy9hcHAvdWkvdGFicy90YWJzLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzlQQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3RCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDZkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzdMQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQy9EQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNmQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDckVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIndXNlIHN0cmljdCc7XG5cbi8vIGxvYWQgZ2xvYmFsIGV2ZW50IG1vZGVsXG53aW5kb3cuRXZlbnQgPSByZXF1aXJlKCdsaWIvZXZlbnQuanMnKTtcblxuLy8gbG9hZCBzZWFyY2hlciBtb2R1bGVcbnJlcXVpcmUoJ3NlYXJjaGVyL3NlYXJjaGVyLmpzJyk7IiwiKGZ1bmN0aW9uKCkge1xuICAgIHZhciBTdHJpbmdIZWxwZXIgPSByZXF1aXJlKCcuL2hlbHBlcnMuanMnKTtcblxuICAgIC8vIGJhc2VDbGFzc1xuICAgIC8vIFRoaXMgaXMgYSBtb2RpZmllZCB2ZXJzaW9uIG9mIEpvaG4gUmVzaWcncyBjbGFzc1xuICAgIC8vIGh0dHA6Ly9lam9obi5vcmcvYmxvZy9zaW1wbGUtamF2YXNjcmlwdC1pbmhlcml0YW5jZS9cbiAgICAvLyBJdCBwcm92aWRlcyBjbGFzcyBsZXZlbCBpbmhlcml0YW5jZSBhbmQgY2FsbGJhY2tzLlxuICAgIC8vXG4gICAgLy8gPT09PT09PT09PT09PT09IEhFTFBFUlMgPT09PT09PT09PT09PT09PT1cbiAgICAvLyBpZiB3ZSBhcmUgaW5pdGlhbGl6aW5nIGEgbmV3IGNsYXNzXG4gICAgdmFyIGluaXRpYWxpemluZyA9IGZhbHNlO1xuICAgIHZhciBtYWtlQXJyYXkgPSAkLm1ha2VBcnJheTtcbiAgICB2YXIgaXNGdW5jdGlvbiA9ICQuaXNGdW5jdGlvbjtcbiAgICB2YXIgaXNBcnJheSA9ICQuaXNBcnJheTtcbiAgICB2YXIgZXh0ZW5kID0gJC5leHRlbmQ7XG4gICAgdmFyIGdldE9iamVjdCA9IFN0cmluZ0hlbHBlci5nZXRPYmplY3Q7XG5cbiAgICB2YXIgY29uY2F0QXJncyA9IGZ1bmN0aW9uKGFyciwgYXJncykge1xuICAgICAgICByZXR1cm4gYXJyLmNvbmNhdChtYWtlQXJyYXkoYXJncykpO1xuICAgIH07XG5cbiAgICAvLyB0ZXN0cyBpZiB3ZSBjYW4gZ2V0IHN1cGVyIGluIC50b1N0cmluZygpXG4gICAgdmFyIGZuVGVzdCA9IC94eXovLnRlc3QoZnVuY3Rpb24oKSB7IHh5ejsgfSkgPyAvXFxiX3N1cGVyXFxiLyA6IC8uKi87XG5cbiAgICAvLyBvdmVyd3JpdGVzIGFuIG9iamVjdCB3aXRoIG1ldGhvZHMsIHNldHMgdXAgX3N1cGVyXG4gICAgLy8gICBuZXdQcm9wcyAtIG5ldyBwcm9wZXJ0aWVzXG4gICAgLy8gICBvbGRQcm9wcyAtIHdoZXJlIHRoZSBvbGQgcHJvcGVydGllcyBtaWdodCBiZVxuICAgIC8vICAgYWRkVG8gLSB3aGF0IHdlIGFyZSBhZGRpbmcgdG9cbiAgICB2YXIgaW5oZXJpdFByb3BzID0gZnVuY3Rpb24obmV3UHJvcHMsIG9sZFByb3BzLCBhZGRUbykge1xuICAgICAgICBhZGRUbyA9IGFkZFRvIHx8IG5ld1Byb3BzO1xuXG4gICAgICAgIGZvciAodmFyIG5hbWUgaW4gbmV3UHJvcHMpIHtcbiAgICAgICAgICAgIC8vIENoZWNrIGlmIHdlJ3JlIG92ZXJ3cml0aW5nIGFuIGV4aXN0aW5nIGZ1bmN0aW9uXG4gICAgICAgICAgICBhZGRUb1tuYW1lXSA9IGlzRnVuY3Rpb24obmV3UHJvcHNbbmFtZV0pICYmXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGlzRnVuY3Rpb24ob2xkUHJvcHNbbmFtZV0pICYmXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGZuVGVzdC50ZXN0KG5ld1Byb3BzW25hbWVdKSA/IChmdW5jdGlvbihuYW1lLCBmbikge1xuICAgICAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHRtcCA9IHRoaXMuX3N1cGVyLFxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0O1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIEFkZCBhIG5ldyAuX3N1cGVyKCkgbWV0aG9kIHRoYXQgaXMgdGhlIHNhbWUgbWV0aG9kXG4gICAgICAgICAgICAgICAgICAgIC8vIGJ1dCBvbiB0aGUgc3VwZXItY2xhc3NcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fc3VwZXIgPSBvbGRQcm9wc1tuYW1lXTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBUaGUgbWV0aG9kIG9ubHkgbmVlZCB0byBiZSBib3VuZCB0ZW1wb3JhcmlseSwgc28gd2VcbiAgICAgICAgICAgICAgICAgICAgLy8gcmVtb3ZlIGl0IHdoZW4gd2UncmUgZG9uZSBleGVjdXRpbmdcbiAgICAgICAgICAgICAgICAgICAgcmV0ID0gZm4uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fc3VwZXIgPSB0bXA7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXQ7XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH0pKG5hbWUsIG5ld1Byb3BzW25hbWVdKSA6IG5ld1Byb3BzW25hbWVdO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIHZhciBTVFJfUFJPVE9UWVBFID0gJ3Byb3RvdHlwZSc7XG5cbiAgICAvKipcbiAgICAgKiBAY2xhc3MgYmFzZUNsYXNzXG4gICAgICovXG4gICAgdmFyIGJhc2VDbGFzcyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCkge1xuICAgICAgICAgICAgcmV0dXJuIGJhc2VDbGFzcy5leHRlbmQuYXBwbHkoYmFzZUNsYXNzLCBhcmd1bWVudHMpO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIC8qIEBTdGF0aWMqL1xuICAgIGV4dGVuZChiYXNlQ2xhc3MsIHtcbiAgICAgICAgcHJveHk6IGZ1bmN0aW9uKGZ1bmNzKSB7XG5cbiAgICAgICAgICAgIC8vYXJncyB0aGF0IHNob3VsZCBiZSBjdXJyaWVkXG4gICAgICAgICAgICB2YXIgYXJncyA9IG1ha2VBcnJheShhcmd1bWVudHMpLFxuICAgICAgICAgICAgICAgIHNlbGY7XG5cbiAgICAgICAgICAgIC8vIGdldCB0aGUgZnVuY3Rpb25zIHRvIGNhbGxiYWNrXG4gICAgICAgICAgICBmdW5jcyA9IGFyZ3Muc2hpZnQoKTtcblxuICAgICAgICAgICAgLy8gaWYgdGhlcmUgaXMgb25seSBvbmUgZnVuY3Rpb24sIG1ha2UgZnVuY3MgaW50byBhbiBhcnJheVxuICAgICAgICAgICAgaWYgKCFpc0FycmF5KGZ1bmNzKSkge1xuICAgICAgICAgICAgICAgIGZ1bmNzID0gW2Z1bmNzXTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8ga2VlcCBhIHJlZmVyZW5jZSB0byB1cyBpbiBzZWxmXG4gICAgICAgICAgICBzZWxmID0gdGhpcztcblxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmdW5jcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgZnVuY3NbaV0gPT09ICdzdHJpbmcnICYmICFpc0Z1bmN0aW9uKHRoaXNbZnVuY3NbaV1dKSkge1xuICAgICAgICAgICAgICAgICAgICB0aHJvdyAoJ2NsYXNzLmpzICcgKyAodGhpcy5mdWxsTmFtZSB8fCB0aGlzLkNsYXNzLmZ1bGxOYW1lKSArICcgZG9lcyBub3QgaGF2ZSBhICcgKyBmdW5jc1tpXSArICdtZXRob2QhJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gY2xhc3NfY2IoKSB7XG4gICAgICAgICAgICAgICAgLy8gYWRkIHRoZSBhcmd1bWVudHMgYWZ0ZXIgdGhlIGN1cnJpZWQgYXJnc1xuICAgICAgICAgICAgICAgIHZhciBjdXIgPSBjb25jYXRBcmdzKGFyZ3MsIGFyZ3VtZW50cyksXG4gICAgICAgICAgICAgICAgICAgIGlzU3RyaW5nLFxuICAgICAgICAgICAgICAgICAgICBsZW5ndGggPSBmdW5jcy5sZW5ndGgsXG4gICAgICAgICAgICAgICAgICAgIGYgPSAwLFxuICAgICAgICAgICAgICAgICAgICBmdW5jO1xuXG4gICAgICAgICAgICAgICAgLy8gZ28gdGhyb3VnaCBlYWNoIGZ1bmN0aW9uIHRvIGNhbGwgYmFja1xuICAgICAgICAgICAgICAgIGZvciAoOyBmIDwgbGVuZ3RoOyBmKyspIHtcbiAgICAgICAgICAgICAgICAgICAgZnVuYyA9IGZ1bmNzW2ZdO1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWZ1bmMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gc2V0IGNhbGxlZCB3aXRoIHRoZSBuYW1lIG9mIHRoZSBmdW5jdGlvbiBvbiBzZWxmICh0aGlzIGlzIGhvdyB0aGlzLnZpZXcgd29ya3MpXG4gICAgICAgICAgICAgICAgICAgIGlzU3RyaW5nID0gdHlwZW9mIGZ1bmMgPT09ICdzdHJpbmcnO1xuICAgICAgICAgICAgICAgICAgICBpZiAoaXNTdHJpbmcgJiYgc2VsZi5fc2V0X2NhbGxlZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5jYWxsZWQgPSBmdW5jO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gY2FsbCB0aGUgZnVuY3Rpb25cbiAgICAgICAgICAgICAgICAgICAgY3VyID0gKGlzU3RyaW5nID8gc2VsZltmdW5jXSA6IGZ1bmMpLmFwcGx5KHNlbGYsIGN1ciB8fCBbXSk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gcGFzcyB0aGUgcmVzdWx0IHRvIHRoZSBuZXh0IGZ1bmN0aW9uIChpZiB0aGVyZSBpcyBhIG5leHQgZnVuY3Rpb24pXG4gICAgICAgICAgICAgICAgICAgIGlmIChmIDwgbGVuZ3RoIC0gMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY3VyID0gIWlzQXJyYXkoY3VyKSB8fCBjdXIuX3VzZV9jYWxsID8gW2N1cl0gOiBjdXI7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gY3VyO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgfSxcblxuICAgICAgICBuZXdJbnN0YW5jZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAvLyBnZXQgYSByYXcgaW5zdGFuY2Ugb2JqZXQgKGluaXQgaXMgbm90IGNhbGxlZClcbiAgICAgICAgICAgIHZhciBpbnN0ID0gdGhpcy5yYXdJbnN0YW5jZSgpLFxuICAgICAgICAgICAgICAgIGFyZ3M7XG5cbiAgICAgICAgICAgIC8vIGNhbGwgc2V0dXAgaWYgdGhlcmUgaXMgYSBzZXR1cFxuICAgICAgICAgICAgaWYgKGluc3Quc2V0dXApIHtcbiAgICAgICAgICAgICAgICBhcmdzID0gaW5zdC5zZXR1cC5hcHBseShpbnN0LCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gY2FsbCBpbml0IGlmIHRoZXJlIGlzIGFuIGluaXQsIGlmIHNldHVwIHJldHVybmVkIGFyZ3MsIHVzZSB0aG9zZSBhcyB0aGUgYXJndW1lbnRzXG4gICAgICAgICAgICBpZiAoaW5zdC5pbml0KSB7XG4gICAgICAgICAgICAgICAgaW5zdC5pbml0LmFwcGx5KGluc3QsIGlzQXJyYXkoYXJncykgPyBhcmdzIDogYXJndW1lbnRzKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGluc3Q7XG4gICAgICAgIH0sXG5cbiAgICAgICAgc2V0dXA6IGZ1bmN0aW9uKGJhc2VDbGFzcywgZnVsbE5hbWUpIHtcbiAgICAgICAgICAgIC8vIHNldCBkZWZhdWx0cyBhcyB0aGUgbWVyZ2VyIG9mIHRoZSBwYXJlbnQgZGVmYXVsdHMgYW5kIHRoaXMgb2JqZWN0J3MgZGVmYXVsdHNcbiAgICAgICAgICAgIHRoaXMuZGVmYXVsdHMgPSBleHRlbmQodHJ1ZSwge30sIGJhc2VDbGFzcy5kZWZhdWx0cywgdGhpcy5kZWZhdWx0cyk7XG4gICAgICAgICAgICByZXR1cm4gYXJndW1lbnRzO1xuICAgICAgICB9LFxuXG4gICAgICAgIHJhd0luc3RhbmNlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIC8vIHByZXZlbnQgcnVubmluZyBpbml0XG4gICAgICAgICAgICBpbml0aWFsaXppbmcgPSB0cnVlO1xuICAgICAgICAgICAgdmFyIGluc3QgPSBuZXcgdGhpcygpO1xuICAgICAgICAgICAgaW5pdGlhbGl6aW5nID0gZmFsc2U7XG4gICAgICAgICAgICAvLyBhbGxvdyBydW5uaW5nIGluaXRcbiAgICAgICAgICAgIHJldHVybiBpbnN0O1xuICAgICAgICB9LFxuXG4gICAgICAgIGV4dGVuZDogZnVuY3Rpb24oZnVsbE5hbWUsIGtsYXNzLCBwcm90bykge1xuICAgICAgICAgICAgLy8gZmlndXJlIG91dCB3aGF0IHdhcyBwYXNzZWQgYW5kIG5vcm1hbGl6ZSBpdFxuICAgICAgICAgICAgaWYgKHR5cGVvZiBmdWxsTmFtZSAhPT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgICBwcm90byA9IGtsYXNzO1xuICAgICAgICAgICAgICAgIGtsYXNzID0gZnVsbE5hbWU7XG4gICAgICAgICAgICAgICAgZnVsbE5hbWUgPSBudWxsO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIXByb3RvKSB7XG4gICAgICAgICAgICAgICAgcHJvdG8gPSBrbGFzcztcbiAgICAgICAgICAgICAgICBrbGFzcyA9IG51bGw7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHByb3RvID0gcHJvdG8gfHwge307XG4gICAgICAgICAgICB2YXIgX3N1cGVyX2NsYXNzID0gdGhpcyxcbiAgICAgICAgICAgICAgICBfc3VwZXIgPSB0aGlzW1NUUl9QUk9UT1RZUEVdLFxuICAgICAgICAgICAgICAgIG5hbWUsIHNob3J0TmFtZSwgbmFtZXNwYWNlLCBwcm90b3R5cGU7XG5cbiAgICAgICAgICAgIC8vIEluc3RhbnRpYXRlIGEgYmFzZSBjbGFzcyAoYnV0IG9ubHkgY3JlYXRlIHRoZSBpbnN0YW5jZSxcbiAgICAgICAgICAgIC8vIGRvbid0IHJ1biB0aGUgaW5pdCBjb25zdHJ1Y3RvcilcbiAgICAgICAgICAgIGluaXRpYWxpemluZyA9IHRydWU7XG4gICAgICAgICAgICBwcm90b3R5cGUgPSBuZXcgdGhpcygpO1xuICAgICAgICAgICAgaW5pdGlhbGl6aW5nID0gZmFsc2U7XG5cbiAgICAgICAgICAgIC8vIENvcHkgdGhlIHByb3BlcnRpZXMgb3ZlciBvbnRvIHRoZSBuZXcgcHJvdG90eXBlXG4gICAgICAgICAgICBpbmhlcml0UHJvcHMocHJvdG8sIF9zdXBlciwgcHJvdG90eXBlKTtcblxuICAgICAgICAgICAgLy8gVGhlIGR1bW15IGNsYXNzIGNvbnN0cnVjdG9yXG4gICAgICAgICAgICBmdW5jdGlvbiBDbGFzcygpIHtcbiAgICAgICAgICAgICAgICAvLyBBbGwgY29uc3RydWN0aW9uIGlzIGFjdHVhbGx5IGRvbmUgaW4gdGhlIGluaXQgbWV0aG9kXG4gICAgICAgICAgICAgICAgaWYgKGluaXRpYWxpemluZykge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdW5kZWZpbmVkO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIC8vIHdlIGFyZSBiZWluZyBjYWxsZWQgdy9vIG5ldywgd2UgYXJlIGV4dGVuZGluZ1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLmNvbnN0cnVjdG9yICE9PSBDbGFzcyAmJiBhcmd1bWVudHMubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vcmV0dXJuIENsYXNzLmV4dGVuZC5hcHBseShDbGFzcywgYXJndW1lbnRzKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGFyZ3VtZW50cy5jYWxsZWUuZXh0ZW5kLmFwcGx5KGFyZ3VtZW50cy5jYWxsZWUsIGFyZ3VtZW50cyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHsgLy93ZSBhcmUgYmVpbmcgY2FsbGVkIHcvIG5ld1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5DbGFzcy5uZXdJbnN0YW5jZS5hcHBseSh0aGlzLkNsYXNzLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gQ29weSBvbGQgc3R1ZmYgb250byBjbGFzc1xuICAgICAgICAgICAgZm9yIChuYW1lIGluIHRoaXMpIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5oYXNPd25Qcm9wZXJ0eShuYW1lKSkge1xuICAgICAgICAgICAgICAgICAgICBDbGFzc1tuYW1lXSA9IHRoaXNbbmFtZV07XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBjb3B5IG5ldyBzdGF0aWMgcHJvcHMgb24gY2xhc3NcbiAgICAgICAgICAgIGluaGVyaXRQcm9wcyhrbGFzcywgdGhpcywgQ2xhc3MpO1xuXG4gICAgICAgICAgICAvLyBkbyBuYW1lc3BhY2Ugc3R1ZmZcbiAgICAgICAgICAgIGlmIChmdWxsTmFtZSkge1xuICAgICAgICAgICAgICAgIHZhciBwYXJ0cyA9IGZ1bGxOYW1lLnNwbGl0KC9cXC4vKSxcbiAgICAgICAgICAgICAgICAgICAgc2hvcnROYW1lID0gcGFydHMucG9wKCksXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnQgPSBnZXRPYmplY3QocGFydHMuam9pbignLicpLCB3aW5kb3csIHRydWUpLFxuICAgICAgICAgICAgICAgICAgICBuYW1lc3BhY2UgPSBjdXJyZW50O1xuXG4gICAgICAgICAgICAgICAgaWYgKGN1cnJlbnRbc2hvcnROYW1lXSkge1xuICAgICAgICAgICAgICAgICAgICB0aHJvdyAoJ2Jhc2VDbGFzcy5qcyBUaGVyZVxcJ3MgYWxyZWFkeSBzb21ldGhpbmcgY2FsbGVkICcgKyBmdWxsTmFtZSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLy8hc3RlYWwtcmVtb3ZlLWVuZFxuICAgICAgICAgICAgICAgIGN1cnJlbnRbc2hvcnROYW1lXSA9IENsYXNzO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBzZXQgdGhpbmdzIHRoYXQgY2FuJ3QgYmUgb3ZlcndyaXR0ZW5cbiAgICAgICAgICAgIGV4dGVuZChDbGFzcywge1xuICAgICAgICAgICAgICAgIHByb3RvdHlwZTogcHJvdG90eXBlLFxuICAgICAgICAgICAgICAgIG5hbWVzcGFjZTogbmFtZXNwYWNlLFxuICAgICAgICAgICAgICAgIHNob3J0TmFtZTogc2hvcnROYW1lLFxuICAgICAgICAgICAgICAgIGNvbnN0cnVjdG9yOiBDbGFzcyxcbiAgICAgICAgICAgICAgICBmdWxsTmFtZTogZnVsbE5hbWVcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAvL21ha2Ugc3VyZSBvdXIgcHJvdG90eXBlIGxvb2tzIG5pY2VcbiAgICAgICAgICAgIENsYXNzW1NUUl9QUk9UT1RZUEVdLkNsYXNzID0gQ2xhc3NbU1RSX1BST1RPVFlQRV0uY29uc3RydWN0b3IgPSBDbGFzcztcblxuICAgICAgICAgICAgLy8gY2FsbCB0aGUgY2xhc3Mgc2V0dXBcbiAgICAgICAgICAgIHZhciBhcmdzID0gQ2xhc3Muc2V0dXAuYXBwbHkoQ2xhc3MsIGNvbmNhdEFyZ3MoW19zdXBlcl9jbGFzc10sIGFyZ3VtZW50cykpO1xuXG4gICAgICAgICAgICAvLyBjYWxsIHRoZSBjbGFzcyBpbml0XG4gICAgICAgICAgICBpZiAoQ2xhc3MuaW5pdCkge1xuICAgICAgICAgICAgICAgIENsYXNzLmluaXQuYXBwbHkoQ2xhc3MsIGFyZ3MgfHwgY29uY2F0QXJncyhbX3N1cGVyX2NsYXNzXSwgYXJndW1lbnRzKSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8qIEBQcm90b3R5cGUqL1xuICAgICAgICAgICAgcmV0dXJuIENsYXNzO1xuICAgICAgICB9XG4gICAgfSk7XG5cbiAgICAvLyBwcm94eSBcInRoaXNcIiBjb250ZXh0IHRvIHRoZSBjYWxsYmFja1xuICAgIGJhc2VDbGFzcy5jYWxsYmFjayA9IGJhc2VDbGFzc1tTVFJfUFJPVE9UWVBFXS5jYWxsYmFjayA9IGJhc2VDbGFzc1tTVFJfUFJPVE9UWVBFXS5wcm94eSA9IGJhc2VDbGFzcy5wcm94eTtcblxuICAgIC8vIGV4cG9ydCBiYXNlIGNsYXNzXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBiYXNlQ2xhc3M7XG59KSgpOyIsIid1c2Ugc3RyaWN0JztcblxuKGZ1bmN0aW9uKCkge1xuICAgIHZhciBDbGFzcyA9IHJlcXVpcmUoJy4vYmFzZUNsYXNzLmpzJyk7XG5cbiAgICAvKipcbiAgICAgKiBAY2xhc3MgQ29udHJvbGxlclxuICAgICAqIEBpbmhlcml0cyBCYXNlIENsYXNzXG4gICAgICovXG4gICAgdmFyIENvbnRyb2xsZXIgPSBDbGFzcyhcbiAgICAvKiBAc3RhdGljICovXG4gICAge30sXG4gICAgLyogQHByb3RvdHlwZSAqL1xuICAgIHtcbiAgICAgICAgc2V0dXA6IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucyA9ICQuZXh0ZW5kKCQuZXh0ZW5kKHRydWUsIHt9LCB0aGlzLkNsYXNzLmRlZmF1bHRzKSwgb3B0aW9ucyk7XG5cbiAgICAgICAgICAgIHJldHVybiBbdGhpcy5zY29wZSwgdGhpcy5vcHRpb25zXS5jb25jYXQoJC5tYWtlQXJyYXkoYXJndW1lbnRzKS5zbGljZSgyKSk7XG4gICAgICAgIH1cbiAgICB9KTtcblxuICAgIG1vZHVsZS5leHBvcnRzID0gQ29udHJvbGxlcjtcbn0pKCk7IiwiJ3VzZSBzdHJpY3QnO1xuXG4oZnVuY3Rpb24oKSB7XG4gICAgdmFyIENsYXNzID0gcmVxdWlyZSgnLi9iYXNlQ2xhc3MuanMnKTtcblxuICAgIHZhciBNb2RlbCA9IENsYXNzKFxuICAgIC8qIEBzdGF0aWMgKi9cbiAgICB7XG4gICAgICAgIGRlZmF1bHRzOiB7XG4gICAgICAgICAgICBjb250cmFjdDogbnVsbFxuICAgICAgICB9XG4gICAgfSxcbiAgICAvKiBAcHJvdG90eXBlICovXG4gICAge1xuICAgICAgICBzZXR1cDogZnVuY3Rpb24oYXR0cmlidXRlcywgb3B0aW9ucykge1xuICAgICAgICAgICAgb3B0aW9ucyA9ICQuZXh0ZW5kKHRydWUsIHRoaXMuZGVmYXVsdHMsIG9wdGlvbnMpO1xuXG4gICAgICAgICAgICAvLyBwb3B1bGF0ZSBpdHNlbGYgd2l0aCBhdHRyaWJ1dGVzXG4gICAgICAgICAgICB2YXIgcHJvcDtcbiAgICAgICAgICAgIGZvciAocHJvcCBpbiBhdHRyaWJ1dGVzKSB7XG4gICAgICAgICAgICAgICAgaWYgKGF0dHJpYnV0ZXMuaGFzT3duUHJvcGVydHkocHJvcCkpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFvcHRpb25zLmNvbnRyYWN0IHx8IChvcHRpb25zLmNvbnRyYWN0ICYmIG9wdGlvbnMuY29udHJhY3QuaW5kZXhPZihwcm9wKSAhPT0gLTEpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzW3Byb3BdID0gYXR0cmlidXRlc1twcm9wXTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0pO1xuXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBNb2RlbDtcbn0pKCk7IiwiJ3VzZSBzdHJpY3QnO1xuXG4oZnVuY3Rpb24oKSB7XG4gICAgdmFyIGJhc2VWaWV3ID0gZnVuY3Rpb24odmlld05hbWUpIHtcbiAgICAgICAgdGhpcy52aWV3TmFtZSA9IHZpZXdOYW1lO1xuICAgIH07XG4gICAgYmFzZVZpZXcucHJvdG90eXBlID0ge1xuICAgICAgICB2aWV3TmFtZTogJycsXG5cbiAgICAgICAgcmVuZGVyOiBmdW5jdGlvbihkYXRhKSB7XG4gICAgICAgICAgICByZXR1cm4gTWVjLlRlbXBsYXRlW3RoaXMudmlld05hbWVdKGRhdGEpO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIG1vZHVsZS5leHBvcnRzID0gYmFzZVZpZXc7XG59KSgpOyIsIid1c2Ugc3RyaWN0JztcblxuKGZ1bmN0aW9uKCkge1xuICAgIG1vZHVsZS5leHBvcnRzID0ge1xuICAgICAgICBzdWJzY3JpYmU6IFB1YlN1Yi5zdWJzY3JpYmUsXG4gICAgICAgIHB1Ymxpc2g6IFB1YlN1Yi5wdWJsaXNoLFxuICAgICAgICB1bnN1YnNjcmliZTogUHViU3ViLnVuc3Vic2NyaWJlXG4gICAgfTtcbn0pKCk7IiwiJ3VzZSBzdHJpY3QnO1xuXG4oZnVuY3Rpb24oKSB7XG4gICAgLy8gU2V2ZXJhbCBvZiB0aGUgbWV0aG9kcyBpbiB0aGlzIHBsdWdpbiB1c2UgY29kZSBhZGFwYXRlZCBmcm9tIFByb3RvdHlwZVxuICAgIC8vICAgUHJvdG90eXBlIEphdmFTY3JpcHQgZnJhbWV3b3JrLCB2ZXJzaW9uIDEuNi4wLjFcbiAgICAvLyAgIChjKSAyMDA1LTIwMDcgU2FtIFN0ZXBoZW5zb25cbiAgICB2YXIgcmVncyA9IHtcbiAgICAgICAgdW5kSGFzaDogL198LS8sXG4gICAgICAgIGNvbG9uczogLzo6LyxcbiAgICAgICAgd29yZHM6IC8oW0EtWl0rKShbQS1aXVthLXpdKS9nLFxuICAgICAgICBsb3dVcDogLyhbYS16XFxkXSkoW0EtWl0pL2csXG4gICAgICAgIGRhc2g6IC8oW2EtelxcZF0pKFtBLVpdKS9nLFxuICAgICAgICByZXBsYWNlcjogL1xceyhbXlxcfV0rKVxcfS9nLFxuICAgICAgICBkb3Q6IC9cXC4vXG4gICAgfSxcblxuICAgIC8vIGdldHMgdGhlIG5leHRQYXJ0IHByb3BlcnR5IGZyb20gY3VycmVudFxuICAgIC8vIGFkZCAtIGlmIHRydWUgYW5kIG5leHRQYXJ0IGRvZXNudCBleGlzdCwgY3JlYXRlIGl0IGFzIGFuIGVtcHR5IG9iamVjdFxuICAgIGdldE5leHQgPSBmdW5jdGlvbihjdXJyZW50LCBuZXh0UGFydCwgYWRkKSB7XG4gICAgICAgIHJldHVybiBjdXJyZW50W25leHRQYXJ0XSAhPT0gdW5kZWZpbmVkID8gY3VycmVudFtuZXh0UGFydF0gOiAoYWRkICYmIChjdXJyZW50W25leHRQYXJ0XSA9IHt9KSk7XG4gICAgfSxcblxuICAgIC8vIHJldHVybnMgdHJ1ZSBpZiB0aGUgb2JqZWN0IGNhbiBoYXZlIHByb3BlcnRpZXMgKG5vIG51bGxzKVxuICAgIGlzQ29udGFpbmVyID0gZnVuY3Rpb24oY3VycmVudCkge1xuICAgICAgICB2YXIgdHlwZSA9IHR5cGVvZiBjdXJyZW50O1xuXG4gICAgICAgIHJldHVybiBjdXJyZW50ICYmICh0eXBlID09PSAnZnVuY3Rpb24nIHx8IHR5cGUgPT09ICdvYmplY3QnKTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogU3RyaW5nSGVscGVyXG4gICAgICovXG4gICAgU3RyaW5nSGVscGVyID0ge1xuICAgICAgICAvKipcbiAgICAgICAgICogQGZ1bmN0aW9uIGdldE9iamVjdFxuICAgICAgICAgKiBHZXRzIGFuIG9iamVjdCBmcm9tIGEgc3RyaW5nLiAgSXQgY2FuIGFsc28gbW9kaWZ5IG9iamVjdHMgb24gdGhlXG4gICAgICAgICAqICdvYmplY3QgcGF0aCcgYnkgcmVtb3Zpbmcgb3IgYWRkaW5nIHByb3BlcnRpZXMuXG4gICAgICAgICAqXG4gICAgICAgICAqICAgICBGb28gPSB7QmFyOiB7WmFyOiB7J1RlZCd9fX1cbiAgICAgICAgICogICAgICQuU3RyaW5nLmdldE9iamVjdCgnRm9vLkJhci5aYXInKSAvLy0+ICdUZWQnXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lIHRoZSBuYW1lIG9mIHRoZSBvYmplY3QgdG8gbG9vayBmb3JcbiAgICAgICAgICogQHBhcmFtIHtBcnJheX0gW3Jvb3RzXSBhbiBhcnJheSBvZiByb290IG9iamVjdHMgdG8gbG9vayBmb3IgdGhlXG4gICAgICAgICAqICAgbmFtZS4gIElmIHJvb3RzIGlzIG5vdCBwcm92aWRlZCwgdGhlIHdpbmRvdyBpcyB1c2VkLlxuICAgICAgICAgKiBAcGFyYW0ge0Jvb2xlYW59IFthZGRdIHRydWUgdG8gYWRkIG1pc3Npbmcgb2JqZWN0cyB0b1xuICAgICAgICAgKiAgdGhlIHBhdGguIGZhbHNlIHRvIHJlbW92ZSBmb3VuZCBwcm9wZXJ0aWVzLiB1bmRlZmluZWQgdG9cbiAgICAgICAgICogIG5vdCBtb2RpZnkgdGhlIHJvb3Qgb2JqZWN0XG4gICAgICAgICAqIEByZXR1cm4ge09iamVjdH0gVGhlIG9iamVjdC5cbiAgICAgICAgICovXG4gICAgICAgIGdldE9iamVjdDogZnVuY3Rpb24obmFtZSwgcm9vdHMsIGFkZCkge1xuICAgICAgICAgICAgLy8gdGhlIHBhcnRzIG9mIHRoZSBuYW1lIHdlIGFyZSBsb29raW5nIHVwXG4gICAgICAgICAgICAvLyBbJ0FwcCcsJ01vZGVscycsJ1JlY2lwZSddXG4gICAgICAgICAgICB2YXIgcGFydHMgPSBuYW1lID8gbmFtZS5zcGxpdChyZWdzLmRvdCkgOiBbXSxcbiAgICAgICAgICAgICAgICBsZW5ndGggPSBwYXJ0cy5sZW5ndGgsXG4gICAgICAgICAgICAgICAgY3VycmVudCxcbiAgICAgICAgICAgICAgICByZXQsXG4gICAgICAgICAgICAgICAgaSxcbiAgICAgICAgICAgICAgICByID0gMDtcblxuICAgICAgICAgICAgLy8gbWFrZSBzdXJlIHJvb3RzIGlzIGFuIGFycmF5XG4gICAgICAgICAgICByb290cyA9ICQuaXNBcnJheShyb290cykgPyByb290cyA6IFtyb290cyB8fCB3aW5kb3ddO1xuXG4gICAgICAgICAgICBpZiAobGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJvb3RzWzBdO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBmb3IgZWFjaCByb290LCBtYXJrIGl0IGFzIGN1cnJlbnRcbiAgICAgICAgICAgIHdoaWxlIChjdXJyZW50ID0gcm9vdHNbcisrXSkge1xuICAgICAgICAgICAgICAgIC8vIHdhbGsgY3VycmVudCB0byB0aGUgMm5kIHRvIGxhc3Qgb2JqZWN0XG4gICAgICAgICAgICAgICAgLy8gb3IgdW50aWwgdGhlcmUgaXMgbm90IGEgY29udGFpbmVyXG4gICAgICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IGxlbmd0aCAtIDEgJiYgaXNDb250YWluZXIoY3VycmVudCk7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICBjdXJyZW50ID0gZ2V0TmV4dChjdXJyZW50LCBwYXJ0c1tpXSwgYWRkKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLy8gaWYgd2UgY2FuIGdldCBhIHByb3BlcnR5IGZyb20gdGhlIDJuZCB0byBsYXN0IG9iamVjdFxuICAgICAgICAgICAgICAgIGlmIChpc0NvbnRhaW5lcihjdXJyZW50KSkge1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGdldCAoYW5kIHBvc3NpYmx5IHNldCkgdGhlIHByb3BlcnR5XG4gICAgICAgICAgICAgICAgICAgIHJldCA9IGdldE5leHQoY3VycmVudCwgcGFydHNbaV0sIGFkZCk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgdGhlcmUgaXMgYSB2YWx1ZSwgd2UgZXhpdFxuICAgICAgICAgICAgICAgICAgICBpZiAocmV0ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGlmIGFkZCBpcyBmYWxzZSwgZGVsZXRlIHRoZSBwcm9wZXJ0eVxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFkZCA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZWxldGUgY3VycmVudFtwYXJ0c1tpXV07XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByZXQ7XG5cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogQ2FwaXRhbGl6ZXMgYSBzdHJpbmdcbiAgICAgICAgICogQHBhcmFtIHtTdHJpbmd9IHMgdGhlIHN0cmluZy5cbiAgICAgICAgICogQHJldHVybiB7U3RyaW5nfSBhIHN0cmluZyB3aXRoIHRoZSBmaXJzdCBjaGFyYWN0ZXIgY2FwaXRhbGl6ZWQuXG4gICAgICAgICAqL1xuICAgICAgICAvKmNhcGl0YWxpemU6IGZ1bmN0aW9uKCBzLCBjYWNoZSApIHtcbiAgICAgICAgICAgIHJldHVybiBzLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcy5zdWJzdHIoMSk7XG4gICAgICAgIH0sKi9cblxuICAgICAgICAvKipcbiAgICAgICAgICogQ2FwaXRhbGl6ZXMgYSBzdHJpbmcgZnJvbSBzb21ldGhpbmcgdW5kZXJjb3JlZC4gRXhhbXBsZXM6XG4gICAgICAgICAqIEBjb2Rlc3RhcnRcbiAgICAgICAgICogalF1ZXJ5LlN0cmluZy5jYW1lbGl6ZSgnb25lX3R3bycpIC8vLT4gJ29uZVR3bydcbiAgICAgICAgICogJ3RocmVlLWZvdXInLmNhbWVsaXplKCkgLy8tPiB0aHJlZUZvdXJcbiAgICAgICAgICogQGNvZGVlbmRcbiAgICAgICAgICogQHBhcmFtIHtTdHJpbmd9IHNcbiAgICAgICAgICogQHJldHVybiB7U3RyaW5nfSBhIHRoZSBjYW1lbGl6ZWQgc3RyaW5nXG4gICAgICAgICAqL1xuICAgICAgICAvKmNhbWVsaXplOiBmdW5jdGlvbihzKSB7XG4gICAgICAgICAgICBzID0gU3RyaW5nSGVscGVyLmNsYXNzaXplKHMpO1xuICAgICAgICAgICAgcmV0dXJuIHMuY2hhckF0KDApLnRvTG93ZXJDYXNlKCkgKyBzLnN1YnN0cigxKTtcbiAgICAgICAgfSwqL1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBMaWtlIFtqUXVlcnkuU3RyaW5nLmNhbWVsaXplfGNhbWVsaXplXSwgYnV0IHRoZSBmaXJzdCBwYXJ0IGlzIGFsc28gY2FwaXRhbGl6ZWRcbiAgICAgICAgICogQHBhcmFtIHtTdHJpbmd9IHNcbiAgICAgICAgICogQHJldHVybiB7U3RyaW5nfSB0aGUgY2xhc3NpemVkIHN0cmluZ1xuICAgICAgICAgKi9cbiAgICAgICAgLypjbGFzc2l6ZSA6IGZ1bmN0aW9uKHMgLCBqb2luKSB7XG4gICAgICAgICAgICB2YXIgcGFydHMgPSBzLnNwbGl0KHJlZ3MudW5kSGFzaCksXG4gICAgICAgICAgICAgICAgaSA9IDA7XG4gICAgICAgICAgICBmb3IgKDsgaSA8IHBhcnRzLmxlbmd0aDsgaSsrICkge1xuICAgICAgICAgICAgICAgIHBhcnRzW2ldID0gU3RyaW5nSGVscGVyLmNhcGl0YWxpemUocGFydHNbaV0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gcGFydHMuam9pbihqb2luIHx8ICcnKTtcbiAgICAgICAgfSwqL1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBMaWtlIFtqUXVlcnkuU3RyaW5nLmNsYXNzaXplfGNsYXNzaXplXSwgYnV0IGEgc3BhY2Ugc2VwYXJhdGVzIGVhY2ggJ3dvcmQnXG4gICAgICAgICAqIEBjb2Rlc3RhcnRcbiAgICAgICAgICogalF1ZXJ5LlN0cmluZy5uaWNlTmFtZSgnb25lX3R3bycpIC8vLT4gJ09uZSBUd28nXG4gICAgICAgICAqIEBjb2RlZW5kXG4gICAgICAgICAqIEBwYXJhbSB7U3RyaW5nfSBzXG4gICAgICAgICAqIEByZXR1cm4ge1N0cmluZ30gdGhlIG5pY2VOYW1lXG4gICAgICAgICAqL1xuICAgICAgICAvKm5pY2VOYW1lIDogZnVuY3Rpb24oIHMgKSB7XG4gICAgICAgICAgICByZXR1cm4gU3RyaW5nSGVscGVyLmNsYXNzaXplKHMsJyAnKTtcbiAgICAgICAgfSwqL1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBVbmRlcnNjb3JlcyBhIHN0cmluZy5cbiAgICAgICAgICogQGNvZGVzdGFydFxuICAgICAgICAgKiBqUXVlcnkuU3RyaW5nLnVuZGVyc2NvcmUoJ09uZVR3bycpIC8vLT4gJ29uZV90d28nXG4gICAgICAgICAqIEBjb2RlZW5kXG4gICAgICAgICAqIEBwYXJhbSB7U3RyaW5nfSBzXG4gICAgICAgICAqIEByZXR1cm4ge1N0cmluZ30gdGhlIHVuZGVyc2NvcmVkIHN0cmluZ1xuICAgICAgICAgKi9cbiAgICAgICAgdW5kZXJzY29yZTogZnVuY3Rpb24ocykge1xuICAgICAgICAgICAgcmV0dXJuIHMucmVwbGFjZShyZWdzLmNvbG9ucywgJy8nKS5yZXBsYWNlKHJlZ3Mud29yZHMsICckMV8kMicpLnJlcGxhY2UocmVncy5sb3dVcCwgJyQxXyQyJykucmVwbGFjZShyZWdzLmRhc2gsICdfJykudG9Mb3dlckNhc2UoKTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogUmV0dXJucyBhIHN0cmluZyB3aXRoIHtwYXJhbX0gcmVwbGFjZWQgdmFsdWVzIGZyb20gZGF0YS5cbiAgICAgICAgICpcbiAgICAgICAgICogICAgICQuU3RyaW5nLnN1YignZm9vIHtiYXJ9Jyx7YmFyOiAnZmFyJ30pXG4gICAgICAgICAqICAgICAvLy0+ICdmb28gZmFyJ1xuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0ge1N0cmluZ30gcyBUaGUgc3RyaW5nIHRvIHJlcGxhY2VcbiAgICAgICAgICogQHBhcmFtIHtPYmplY3R9IGRhdGEgVGhlIGRhdGEgdG8gYmUgdXNlZCB0byBsb29rIGZvciBwcm9wZXJ0aWVzLiAgSWYgaXQncyBhbiBhcnJheSwgbXVsdGlwbGVcbiAgICAgICAgICogb2JqZWN0cyBjYW4gYmUgdXNlZC5cbiAgICAgICAgICogQHBhcmFtIHtCb29sZWFufSBbcmVtb3ZlXSBpZiBhIG1hdGNoIGlzIGZvdW5kLCByZW1vdmUgdGhlIHByb3BlcnR5IGZyb20gdGhlIG9iamVjdFxuICAgICAgICAgKi9cbiAgICAgICAgLypzdWI6IGZ1bmN0aW9uKHMsIGRhdGEsIHJlbW92ZSkge1xuICAgICAgICAgICAgdmFyIG9icyA9IFtdLFxuICAgICAgICAgICAgICAgIHJlbW92ZSA9IHR5cGVvZiByZW1vdmUgPT0gJ2Jvb2xlYW4nID8gIXJlbW92ZSA6IHJlbW92ZTtcbiAgICAgICAgICAgIG9icy5wdXNoKHMucmVwbGFjZShyZWdzLnJlcGxhY2VyLCBmdW5jdGlvbih3aG9sZSwgaW5zaWRlKSB7XG4gICAgICAgICAgICAgICAgLy9jb252ZXJ0IGluc2lkZSB0byB0eXBlXG4gICAgICAgICAgICAgICAgdmFyIG9iID0gU3RyaW5nSGVscGVyLmdldE9iamVjdChpbnNpZGUsIGRhdGEsIHJlbW92ZSk7XG5cbiAgICAgICAgICAgICAgICAvLyBpZiBhIGNvbnRhaW5lciwgcHVzaCBpbnRvIG9ianMgKHdoaWNoIHdpbGwgcmV0dXJuIG9iamVjdHMgZm91bmQpXG4gICAgICAgICAgICAgICAgaWYoaXNDb250YWluZXIob2IpKSB7XG4gICAgICAgICAgICAgICAgICAgIG9icy5wdXNoKG9iKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICcnO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnJyArIG9iO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pKTtcblxuICAgICAgICAgICAgcmV0dXJuIG9icy5sZW5ndGggPD0gMSA/IG9ic1swXSA6IG9icztcbiAgICAgICAgfSwqL1xuXG4gICAgICAgIF9yZWdzOiByZWdzXG4gICAgfTtcblxuICAgIG1vZHVsZS5leHBvcnRzID0gU3RyaW5nSGVscGVyO1xufSkoKTsiLCIndXNlIHN0cmljdCc7XG5cbihmdW5jdGlvbigpIHtcbiAgICB2YXIgTW9kZWwgPSByZXF1aXJlKCdsaWIvYmFzZU1vZGVsLmpzJyk7XG5cbiAgICBNb2RlbCgnTWVjLk1vZGVscy5Qcm9kdWN0Jywge30sIHt9KTtcbn0pKCk7IiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgQ29udHJvbGxlciA9IHJlcXVpcmUoJ2xpYi9iYXNlQ29udHJvbGxlci5qcycpO1xudmFyIFZpZXcgPSByZXF1aXJlKCdsaWIvYmFzZVZpZXcuanMnKTtcblxucmVxdWlyZSgnc2VydmljZXMvcHJvZHVjdC5qcycpO1xuXG4vKipcbiAqIEBjbGFzcyBNZWMuU2VhcmNoZXIuUHJvZHVjdHNcbiAqIEBpbmhlcml0cyBDb250cm9sbGVyXG4gKi9cbm1vZHVsZS5leHBvcnRzID0gQ29udHJvbGxlcignTWVjLlNlYXJjaGVyLlByb2R1Y3RzJyxcbi8qIEBTdGF0aWMgKi9cbntcbiAgICBkZWZhdWx0czoge1xuICAgICAgICBzZWFyY2hFdmVudDogJ3NlYXJjaGVyLnNlYXJjaCcsXG4gICAgICAgIHZpZXc6IG5ldyBWaWV3KCdzZWFyY2hlclByb2R1Y3RzJylcbiAgICB9XG59LFxuLyogQFByb3RvdHlwZSAqL1xue1xuICAgIGluaXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgICAgIHZhciBvcHRpb25zID0gc2VsZi5vcHRpb25zO1xuXG4gICAgICAgIC8vIGxpc3RlbiBmb3Igc2VhcmNoIGV2ZW50XG4gICAgICAgIEV2ZW50LnN1YnNjcmliZShvcHRpb25zLnNlYXJjaEV2ZW50LCB0aGlzLmNhbGxiYWNrKCdzZWFyY2hTdWJzY3JpYmVyJykpO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBSZXF1ZXN0cyBwcm9kdWN0cyBmb3IgcHJvdmlkZWQgcXVlcnlcbiAgICAgKi9cbiAgICBzZWFyY2hTdWJzY3JpYmVyOiBmdW5jdGlvbihldiwgc2VhcmNoUXVlcnkpIHtcbiAgICAgICAgdmFyIHByb2R1Y3RzTGlzdCA9IE1lYy5TZXJ2aWNlcy5Qcm9kdWN0LmZpbmRBbGwoe1xuICAgICAgICAgICAga2V5d29yZHM6IHNlYXJjaFF1ZXJ5XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHByb2R1Y3RzTGlzdC50aGVuKHRoaXMuY2FsbGJhY2soJ3JlbmRlcicpKTtcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogUmVuZGVyIHByb2R1Y3RzIGludG8gdmlld1xuICAgICAqL1xuICAgIHJlbmRlcjogZnVuY3Rpb24oZGF0YSkge1xuICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgICAgIHZhciB2aWV3ID0gc2VsZi5vcHRpb25zLnZpZXc7XG5cbiAgICAgICAgdGhpcy5zY29wZS5odG1sKHZpZXcucmVuZGVyKGRhdGEpKTtcbiAgICB9XG59KTtcblxuLy8gY3JlYXRlIGEganF1ZXJ5IHBsdWdpblxuJC5mbi5tZWNfc2VhcmNoZXJfcHJvZHVjdHMgPSBmdW5jdGlvbihvcHRpb25zKSB7XG4gICAgb3B0aW9ucyA9IChvcHRpb25zID8gb3B0aW9ucyA6IHt9KTtcblxuICAgIHJldHVybiBuZXcgTWVjLlNlYXJjaGVyLlByb2R1Y3RzKHRoaXMsIG9wdGlvbnMpO1xufTsiLCIndXNlIHN0cmljdCc7XG5cbnZhciBDb250cm9sbGVyID0gcmVxdWlyZSgnbGliL2Jhc2VDb250cm9sbGVyLmpzJyk7XG5cbkNvbnRyb2xsZXIoJ01lYy5TZWFyY2hlci5TZWFyY2gnLFxuLyogQFN0YXRpYyAqL1xue1xuICAgIGRlZmF1bHRzOiB7XG4gICAgICAgIHNlbGVjdG9yczoge1xuICAgICAgICAgICAgc2VhcmNoRmllbGQ6ICcjc2VhcmNoLWZpZWxkJyxcbiAgICAgICAgICAgIHNlYXJjaFRyaWdnZXI6ICcjc2VhcmNoLXRyaWdnZXInXG4gICAgICAgIH0sXG4gICAgICAgIHF1ZXJ5OiAnJyxcbiAgICAgICAgc2VhcmNoRXZlbnQ6ICdzZWFyY2hlci5zZWFyY2gnXG4gICAgfVxufSxcbi8qIEBQcm90b3R5cGUgKi9cbntcbiAgICBpbml0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgICAgICB2YXIgc2VsZWN0b3JzID0gc2VsZi5vcHRpb25zLnNlbGVjdG9ycztcblxuICAgICAgICAvLyBiaW5kIHRvIGV2ZW50c1xuICAgICAgICBzZWxmLnNjb3BlLmZpbmQoc2VsZWN0b3JzLnNlYXJjaEZpZWxkKS5vbigna2V5dXAnLCBmdW5jdGlvbihldikge1xuICAgICAgICAgICAgc2VsZi5oYW5kbGVGaWVsZEtleXVwKCQodGhpcyksIGV2KTtcbiAgICAgICAgfSk7XG4gICAgICAgIHNlbGYuc2NvcGUuZmluZChzZWxlY3RvcnMuc2VhcmNoVHJpZ2dlcikub24oJ2NsaWNrJywgZnVuY3Rpb24oZXYpIHtcbiAgICAgICAgICAgIHNlbGYuaGFuZGxlVHJpZ2dlckNsaWNrKCQodGhpcyksIGV2KTtcbiAgICAgICAgfSk7XG4gICAgfSxcblxuICAgIGhhbmRsZUZpZWxkS2V5dXA6IGZ1bmN0aW9uKGVsLCBldikge1xuICAgICAgICBpZiAoZXYud2hpY2ggPT09IDEzKSB7XG4gICAgICAgICAgICB2YXIgcXVlcnkgPSBlbC52YWwoKTtcblxuICAgICAgICAgICAgdGhpcy5zZWFyY2gocXVlcnkpO1xuICAgICAgICB9XG4gICAgfSxcblxuICAgIGhhbmRsZVRyaWdnZXJDbGljazogZnVuY3Rpb24oZWwsIGV2KSB7XG4gICAgICAgIHZhciBzZWFyY2hGaWVsZEVsID0gdGhpcy5zY29wZS5maW5kKHRoaXMub3B0aW9ucy5zZWxlY3RvcnMuc2VhcmNoRmllbGQpO1xuICAgICAgICB2YXIgcXVlcnkgPSBzZWFyY2hGaWVsZEVsLnZhbCgpO1xuXG4gICAgICAgIHRoaXMuc2VhcmNoKHF1ZXJ5KTtcbiAgICB9LFxuXG4gICAgc2VhcmNoOiBmdW5jdGlvbihuZXdRdWVyeSkge1xuICAgICAgICB2YXIgY3VycmVudFF1ZXJ5ID0gdGhpcy5vcHRpb25zLnF1ZXJ5O1xuXG4gICAgICAgIGlmIChuZXdRdWVyeSAhPT0gY3VycmVudFF1ZXJ5ICYmIG5ld1F1ZXJ5ICE9PSAnJykge1xuICAgICAgICAgICAgdGhpcy5vcHRpb25zLnF1ZXJ5ID0gbmV3UXVlcnk7XG5cbiAgICAgICAgICAgIC8vIHRyaWdnZXIgc2VhcmNoIGV2ZW50XG4gICAgICAgICAgICBFdmVudC5wdWJsaXNoKHRoaXMub3B0aW9ucy5zZWFyY2hFdmVudCwgbmV3UXVlcnkpO1xuICAgICAgICB9XG4gICAgfVxufSk7XG5cbi8vIGNyZWF0ZSBhIGpxdWVyeSBwbHVnaW5cbiQuZm4ubWVjX3NlYXJjaGVyX3NlYXJjaCA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcbiAgICBvcHRpb25zID0gKG9wdGlvbnMgPyBvcHRpb25zIDoge30pO1xuXG4gICAgcmV0dXJuIG5ldyBNZWMuU2VhcmNoZXIuU2VhcmNoKHRoaXMsIG9wdGlvbnMpO1xufTsiLCIndXNlIHN0cmljdCc7XG5cbnJlcXVpcmUoJ3NlYXJjaGVyL3Byb2R1Y3RzL3Byb2R1Y3RzLmpzJyk7XG5yZXF1aXJlKCdzZWFyY2hlci9zZWFyY2gvc2VhcmNoLmpzJyk7XG5yZXF1aXJlKCdzZWFyY2hlci90YWJzL3RhYnMuanMnKTtcblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgLy8gaW5pdCBzZWFyY2hcbiAgICAkKCcjc2VhcmNoZXInKS5tZWNfc2VhcmNoZXJfc2VhcmNoKCk7XG5cbiAgICAvLyBpbml0IHByb2R1Y3RzXG4gICAgJCgnI3Byb2R1Y3RzLWNvbnRhaW5lcicpLm1lY19zZWFyY2hlcl9wcm9kdWN0cygpO1xuXG4gICAgLy8gaW5pdCB0YWJzIChubyBwbHVnaW4pXG4gICAgbmV3IE1lYy5TZWFyY2hlci5UYWJzKCcjc2VhcmNoZXItdGFicycpO1xufSk7IiwiLyogZ2xvYmFscyBVaSAqL1xuJ3VzZSBzdHJpY3QnO1xuXG4oZnVuY3Rpb24oKSB7XG4gICAgcmVxdWlyZSgndWkvdGFicy90YWJzLmpzJyk7XG5cbiAgICAvKipcbiAgICAgKiBAY2xhc3MgTWVjLlNlYXJjaGVyLlRhYnNcbiAgICAgKiBAaW5oZXJpdHMgVWkuVGFic1xuICAgICAqL1xuICAgIFVpLlRhYnMoJ01lYy5TZWFyY2hlci5UYWJzJywge30sIHt9KTtcbn0pKCk7IiwiJ3VzZSBzdHJpY3QnO1xuXG4oZnVuY3Rpb24oKSB7XG4gICAgdmFyIENsYXNzID0gcmVxdWlyZSgnbGliL2Jhc2VDbGFzcy5qcycpO1xuICAgIHJlcXVpcmUoJ21vZGVscy9wcm9kdWN0LmpzJyk7XG5cbiAgICBDbGFzcygnTWVjLlNlcnZpY2VzLlByb2R1Y3QnLFxuICAgIC8qIEBTdGF0aWMgKi9cbiAgICB7XG4gICAgICAgIGRlZmF1bHRzOiB7XG4gICAgICAgICAgICBkb21haW46ICdodHRwOi8vd3d3Lm1lYy5jYScsXG4gICAgICAgICAgICBhcGlQYXRoOiAnL2FwaS92MScsXG4gICAgICAgICAgICB1cmw6ICcvcHJvZHVjdHMvc2VhcmNoJyxcbiAgICAgICAgICAgIG1vZGVsOiBNZWMuTW9kZWxzLlByb2R1Y3QsXG4gICAgICAgICAgICBjb250cmFjdDogbnVsbFxuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBQcm9kdWN0cyBsb29rdXBcbiAgICAgICAgICovXG4gICAgICAgIGZpbmRBbGw6IGZ1bmN0aW9uKHBhcmFtcywgc3VjY2VzcywgZXJyb3IpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnJlc291cmNlKHBhcmFtcywgc3VjY2VzcywgZXJyb3IpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBNYWtlIGFuIGFqYXggcmVxdWVzdCBhbmQgcmV0dXJuIGEgcHJvbWlzZVxuICAgICAgICAgKi9cbiAgICAgICAgcmVzb3VyY2U6IGZ1bmN0aW9uKHBhcmFtcywgc3VjY2VzcywgZXJyb3IpIHtcbiAgICAgICAgICAgIHZhciBvcHRpb25zID0gdGhpcy5kZWZhdWx0cztcbiAgICAgICAgICAgIHZhciB1cmwgPSBvcHRpb25zLmRvbWFpbiArIG9wdGlvbnMuYXBpUGF0aCArIG9wdGlvbnMudXJsO1xuXG4gICAgICAgICAgICAvLyBzZXQgY29udHJhY3QgZm9yIHRoZSByZXNvdXJjZVxuICAgICAgICAgICAgdGhpcy5jb250cmFjdCA9IChwYXJhbXMuY29udHJhY3QgPyBwYXJhbXMuY29udHJhY3QgOiBudWxsKTtcblxuICAgICAgICAgICAgLy8gbWVyZ2UgcGFyYW1zXG4gICAgICAgICAgICBwYXJhbXMgPSAkLmV4dGVuZCh0cnVlLCB7XG4gICAgICAgICAgICAgICAga2V5d29yZHM6ICcnXG4gICAgICAgICAgICB9LCBwYXJhbXMpO1xuXG4gICAgICAgICAgICByZXR1cm4gJC5hamF4KHVybCwge1xuICAgICAgICAgICAgICAgIGRhdGE6IHBhcmFtc1xuICAgICAgICAgICAgfSkudGhlbih0aGlzLmNhbGxiYWNrKCdoeWRyYXRlJykpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBIeWRyYXRlIGpzb24gd2l0aCBtb2RlbCBpbnN0YW5jZXNcbiAgICAgICAgICovXG4gICAgICAgIGh5ZHJhdGU6IGZ1bmN0aW9uKGRhdGEpIHtcbiAgICAgICAgICAgIHZhciBvcHRpb25zID0gdGhpcy5kZWZhdWx0cztcblxuICAgICAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5wcm9kdWN0cykge1xuICAgICAgICAgICAgICAgIHZhciBwcm9kdWN0cyA9IFtdO1xuXG4gICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IGRhdGEucHJvZHVjdHMubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHByb2R1Y3QgPSBuZXcgb3B0aW9ucy5tb2RlbChkYXRhLnByb2R1Y3RzW2ldLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb250cmFjdDogb3B0aW9ucy5jb250cmFjdFxuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICBwcm9kdWN0cy5wdXNoKHByb2R1Y3QpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGRhdGEucHJvZHVjdHMgPSBwcm9kdWN0cztcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGRhdGE7XG4gICAgICAgIH1cbiAgICB9LFxuICAgIC8qIEBQcm90b3R5cGUgKi9cbiAgICB7fSk7XG59KSgpOyIsIid1c2Ugc3RyaWN0JztcblxuKGZ1bmN0aW9uKCkge1xuICAgIHZhciBDb250cm9sbGVyID0gcmVxdWlyZSgnbGliL2Jhc2VDb250cm9sbGVyLmpzJyk7XG5cbiAgICAvKipcbiAgICAgKiBAY2xhc3MgVWkuVGFic1xuICAgICAqIEBpbmhlcml0cyBDb250cm9sbGVyXG4gICAgICovXG4gICAgQ29udHJvbGxlcignVWkuVGFicycsXG4gICAgLyoqIEBTdGF0aWMgKi9cbiAgICB7XG4gICAgICAgIGRlZmF1bHRzOiB7XG4gICAgICAgICAgICBzZWxlY3RvcnM6IHtcbiAgICAgICAgICAgICAgICB0YWI6ICcudWktdGFiJyxcbiAgICAgICAgICAgICAgICBjb250ZW50OiAnLnVpLXRhYi1jb250ZW50J1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGFjdGl2ZUNsYXNzOiAnYWN0aXZlJyxcbiAgICAgICAgICAgIG9uSW5pdDogZnVuY3Rpb24oKSB7fSxcbiAgICAgICAgICAgIG9uVGFiQ2xpY2s6IGZ1bmN0aW9uKCkge30sXG4gICAgICAgICAgICBvbkNvbnRlbnRTaG93OiBmdW5jdGlvbigpIHt9XG4gICAgICAgIH1cbiAgICB9LFxuICAgIC8qKiBAUHJvdG90eXBlICovXG4gICAge1xuICAgICAgICAvKipcbiAgICAgICAgICogTWFpbiBJbml0XG4gICAgICAgICAqL1xuICAgICAgICBpbml0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBzZWxmID0gdGhpcztcbiAgICAgICAgICAgIHZhciBvcHRpb25zID0gc2VsZi5vcHRpb25zO1xuICAgICAgICAgICAgdmFyIHRhYiA9IHNlbGYuc2NvcGUuZmluZChvcHRpb25zLnNlbGVjdG9ycy50YWIpO1xuICAgICAgICAgICAgdmFyIGNvbnRlbnQgPSBzZWxmLnNjb3BlLmZpbmQob3B0aW9ucy5zZWxlY3RvcnMuY29udGVudCk7XG5cbiAgICAgICAgICAgIC8vIFRyaWdnZXIgaW5pdGlhbCB0YWJcbiAgICAgICAgICAgIGlmICh0YWIuZmlsdGVyKCcuJyArIG9wdGlvbnMuYWN0aXZlQ2xhc3MpLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgICAgIHRhYi5maWx0ZXIoJzpmaXJzdCcpLmFkZENsYXNzKG9wdGlvbnMuYWN0aXZlQ2xhc3MpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3Mob3B0aW9ucy5hY3RpdmVDbGFzcyk7XG4gICAgICAgICAgICAgICAgY29udGVudC5maWx0ZXIoJzpmaXJzdCcpLmFkZENsYXNzKG9wdGlvbnMuYWN0aXZlQ2xhc3MpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3Mob3B0aW9ucy5hY3RpdmVDbGFzcyk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHZhciBpbmRleCA9IHRhYi5maWx0ZXIoJy4nICsgb3B0aW9ucy5hY3RpdmVDbGFzcykuaW5kZXgoKTtcbiAgICAgICAgICAgICAgICBjb250ZW50LmVxKGluZGV4KS5hZGRDbGFzcyhvcHRpb25zLmFjdGl2ZUNsYXNzKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKG9wdGlvbnMuYWN0aXZlQ2xhc3MpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBDYWxsIG9uSW5pdCBjYWxsYmFja1xuICAgICAgICAgICAgb3B0aW9ucy5vbkluaXQoc2VsZi5zY29wZSwgb3B0aW9ucyk7XG5cbiAgICAgICAgICAgIC8vIGV2ZW50IGxpc3RlbmVyc1xuICAgICAgICAgICAgc2VsZi5zY29wZS5vbignY2xpY2snLCBvcHRpb25zLnNlbGVjdG9ycy50YWIsIGZ1bmN0aW9uKGV2KSB7XG4gICAgICAgICAgICAgICAgc2VsZi50YWJDbGlja0hhbmRsZXIoJCh0aGlzKSwgZXYpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFRhYiBjbGljayBoYW5kbGVyXG4gICAgICAgICAqL1xuICAgICAgICB0YWJDbGlja0hhbmRsZXI6IGZ1bmN0aW9uKGVsLCBldikge1xuICAgICAgICAgICAgZXYucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgICAgICAgICAgdmFyIG9wdGlvbnMgPSBzZWxmLm9wdGlvbnM7XG5cbiAgICAgICAgICAgIC8vIENhbGwgb25UYWJDbGljayBjYWxsYmFja1xuICAgICAgICAgICAgb3B0aW9ucy5vblRhYkNsaWNrKGVsLCBldik7XG5cbiAgICAgICAgICAgIC8vIFNldCBhY3RpdmUgY2xhc3MgZm9yIHNlbGVjdGVkIHRhYiBiYXNlZCBvbiBpbmRleFxuICAgICAgICAgICAgZWwuYWRkQ2xhc3Mob3B0aW9ucy5hY3RpdmVDbGFzcykuc2libGluZ3MoKS5yZW1vdmVDbGFzcyhvcHRpb25zLmFjdGl2ZUNsYXNzKTtcblxuICAgICAgICAgICAgLy8gU2V0IGFjdGl2ZSBjbGFzcyBmb3Igc2VsZWN0ZWQgY29udGVudCBiYXNlZCBvbiB0YWIgaW5kZXhcbiAgICAgICAgICAgIHZhciBjb250ZW50ID0gc2VsZi5zY29wZS5maW5kKG9wdGlvbnMuc2VsZWN0b3JzLmNvbnRlbnQpLmVxKGVsLmluZGV4KCkpO1xuICAgICAgICAgICAgc2VsZi5jb250ZW50U2hvdyhjb250ZW50KTtcbiAgICAgICAgfSxcblxuICAgICAgICAvKipcbiAgICAgICAgICogQ29udGVudCBkaXNwbGF5XG4gICAgICAgICAqL1xuICAgICAgICBjb250ZW50U2hvdzogZnVuY3Rpb24oZWwpIHtcbiAgICAgICAgICAgIHZhciBzZWxmID0gdGhpcztcbiAgICAgICAgICAgIHZhciBvcHRpb25zID0gc2VsZi5vcHRpb25zO1xuXG4gICAgICAgICAgICAvLyBTZXQgYWN0aXZlIGNsYXNzIGZvciBzZWxlY3RlZCB0YWIgYmFzZWQgb24gaW5kZXhcbiAgICAgICAgICAgIGVsLmFkZENsYXNzKG9wdGlvbnMuYWN0aXZlQ2xhc3MpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3Mob3B0aW9ucy5hY3RpdmVDbGFzcyk7XG5cbiAgICAgICAgICAgIC8vIENhbGwgb25Db250ZW50U2hvdyBjYWxsYmFja1xuICAgICAgICAgICAgb3B0aW9ucy5vbkNvbnRlbnRTaG93KGVsKTtcbiAgICAgICAgfVxuICAgIH0pO1xufSkoKTsiXX0=
