this["Mec"] = this["Mec"] || {};
this["Mec"]["Template"] = this["Mec"]["Template"] || {};
this["Mec"]["Template"]["searcherProducts"] = Handlebars.template({"1":function(depth0,helpers,partials,data) {
    var helper;

  return "    <li class=\"searcher-products-item\">"
    + this.escapeExpression(((helper = (helper = helpers.full_name || (depth0 != null ? depth0.full_name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0,{"name":"full_name","hash":{},"data":data}) : helper)))
    + "</li>\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var stack1;

  return "<ul class=\"searcher-products-list\">\n"
    + ((stack1 = helpers.each.call(depth0,(depth0 != null ? depth0.products : depth0),{"name":"each","hash":{},"fn":this.program(1, data, 0),"inverse":this.noop,"data":data})) != null ? stack1 : "")
    + "</ul>";
},"useData":true});