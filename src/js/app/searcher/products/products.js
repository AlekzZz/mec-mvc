'use strict';

var Controller = require('lib/baseController.js');
var View = require('lib/baseView.js');

require('services/product.js');

/**
 * @class Mec.Searcher.Products
 * @inherits Controller
 */
module.exports = Controller('Mec.Searcher.Products',
/* @Static */
{
    defaults: {
        searchEvent: 'searcher.search',
        view: new View('searcherProducts')
    }
},
/* @Prototype */
{
    init: function() {
        var self = this;
        var options = self.options;

        // listen for search event
        Event.subscribe(options.searchEvent, this.callback('searchSubscriber'));
    },

    /**
     * Requests products for provided query
     */
    searchSubscriber: function(ev, searchQuery) {
        var productsList = Mec.Services.Product.findAll({
            keywords: searchQuery
        });

        productsList.then(this.callback('render'));
    },

    /**
     * Render products into view
     */
    render: function(data) {
        var self = this;
        var view = self.options.view;

        this.scope.html(view.render(data));
    }
});

// create a jquery plugin
$.fn.mec_searcher_products = function(options) {
    options = (options ? options : {});

    return new Mec.Searcher.Products(this, options);
};