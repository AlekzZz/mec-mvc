'use strict';

var Controller = require('lib/baseController.js');

Controller('Mec.Searcher.Search',
/* @Static */
{
    defaults: {
        selectors: {
            searchField: '#search-field',
            searchTrigger: '#search-trigger'
        },
        query: '',
        searchEvent: 'searcher.search'
    }
},
/* @Prototype */
{
    init: function() {
        var self = this;
        var selectors = self.options.selectors;

        // bind to events
        self.scope.find(selectors.searchField).on('keyup', function(ev) {
            self.handleFieldKeyup($(this), ev);
        });
        self.scope.find(selectors.searchTrigger).on('click', function(ev) {
            self.handleTriggerClick($(this), ev);
        });
    },

    handleFieldKeyup: function(el, ev) {
        if (ev.which === 13) {
            var query = el.val();

            this.search(query);
        }
    },

    handleTriggerClick: function(el, ev) {
        var searchFieldEl = this.scope.find(this.options.selectors.searchField);
        var query = searchFieldEl.val();

        this.search(query);
    },

    search: function(newQuery) {
        var currentQuery = this.options.query;

        if (newQuery !== currentQuery && newQuery !== '') {
            this.options.query = newQuery;

            // trigger search event
            Event.publish(this.options.searchEvent, newQuery);
        }
    }
});

// create a jquery plugin
$.fn.mec_searcher_search = function(options) {
    options = (options ? options : {});

    return new Mec.Searcher.Search(this, options);
};