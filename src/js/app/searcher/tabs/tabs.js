/* globals Ui */
'use strict';

(function() {
    require('ui/tabs/tabs.js');

    /**
     * @class Mec.Searcher.Tabs
     * @inherits Ui.Tabs
     */
    Ui.Tabs('Mec.Searcher.Tabs', {}, {});
})();