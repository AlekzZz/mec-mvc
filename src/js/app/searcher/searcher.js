'use strict';

require('searcher/products/products.js');
require('searcher/search/search.js');
require('searcher/tabs/tabs.js');

$(document).ready(function() {
    // init search
    $('#searcher').mec_searcher_search();

    // init products
    $('#products-container').mec_searcher_products();

    // init tabs (no plugin)
    new Mec.Searcher.Tabs('#searcher-tabs');
});