'use strict';

(function() {
    var Class = require('./baseClass.js');

    var Model = Class(
    /* @static */
    {
        defaults: {
            contract: null
        }
    },
    /* @prototype */
    {
        setup: function(attributes, options) {
            options = $.extend(true, this.defaults, options);

            // populate itself with attributes
            var prop;
            for (prop in attributes) {
                if (attributes.hasOwnProperty(prop)) {
                    if (!options.contract || (options.contract && options.contract.indexOf(prop) !== -1)) {
                        this[prop] = attributes[prop];
                    }
                }
            }
        }
    });

    module.exports = Model;
})();