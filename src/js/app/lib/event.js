'use strict';

(function() {
    module.exports = {
        subscribe: PubSub.subscribe,
        publish: PubSub.publish,
        unsubscribe: PubSub.unsubscribe
    };
})();