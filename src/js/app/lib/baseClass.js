(function() {
    var StringHelper = require('./helpers.js');

    // baseClass
    // This is a modified version of John Resig's class
    // http://ejohn.org/blog/simple-javascript-inheritance/
    // It provides class level inheritance and callbacks.
    //
    // =============== HELPERS =================
    // if we are initializing a new class
    var initializing = false;
    var makeArray = $.makeArray;
    var isFunction = $.isFunction;
    var isArray = $.isArray;
    var extend = $.extend;
    var getObject = StringHelper.getObject;

    var concatArgs = function(arr, args) {
        return arr.concat(makeArray(args));
    };

    // tests if we can get super in .toString()
    var fnTest = /xyz/.test(function() { xyz; }) ? /\b_super\b/ : /.*/;

    // overwrites an object with methods, sets up _super
    //   newProps - new properties
    //   oldProps - where the old properties might be
    //   addTo - what we are adding to
    var inheritProps = function(newProps, oldProps, addTo) {
        addTo = addTo || newProps;

        for (var name in newProps) {
            // Check if we're overwriting an existing function
            addTo[name] = isFunction(newProps[name]) &&
                          isFunction(oldProps[name]) &&
                          fnTest.test(newProps[name]) ? (function(name, fn) {
                return function() {
                    var tmp = this._super,
                        ret;

                    // Add a new ._super() method that is the same method
                    // but on the super-class
                    this._super = oldProps[name];

                    // The method only need to be bound temporarily, so we
                    // remove it when we're done executing
                    ret = fn.apply(this, arguments);
                    this._super = tmp;
                    return ret;
                };
            })(name, newProps[name]) : newProps[name];
        }
    };

    var STR_PROTOTYPE = 'prototype';

    /**
     * @class baseClass
     */
    var baseClass = function() {
        if (arguments.length) {
            return baseClass.extend.apply(baseClass, arguments);
        }
    };

    /* @Static*/
    extend(baseClass, {
        proxy: function(funcs) {

            //args that should be curried
            var args = makeArray(arguments),
                self;

            // get the functions to callback
            funcs = args.shift();

            // if there is only one function, make funcs into an array
            if (!isArray(funcs)) {
                funcs = [funcs];
            }

            // keep a reference to us in self
            self = this;

            for (var i = 0; i < funcs.length; i++) {
                if (typeof funcs[i] === 'string' && !isFunction(this[funcs[i]])) {
                    throw ('class.js ' + (this.fullName || this.Class.fullName) + ' does not have a ' + funcs[i] + 'method!');
                }
            }

            return function class_cb() {
                // add the arguments after the curried args
                var cur = concatArgs(args, arguments),
                    isString,
                    length = funcs.length,
                    f = 0,
                    func;

                // go through each function to call back
                for (; f < length; f++) {
                    func = funcs[f];
                    if (!func) {
                        continue;
                    }

                    // set called with the name of the function on self (this is how this.view works)
                    isString = typeof func === 'string';
                    if (isString && self._set_called) {
                        self.called = func;
                    }

                    // call the function
                    cur = (isString ? self[func] : func).apply(self, cur || []);

                    // pass the result to the next function (if there is a next function)
                    if (f < length - 1) {
                        cur = !isArray(cur) || cur._use_call ? [cur] : cur;
                    }
                }

                return cur;
            };
        },

        newInstance: function() {
            // get a raw instance objet (init is not called)
            var inst = this.rawInstance(),
                args;

            // call setup if there is a setup
            if (inst.setup) {
                args = inst.setup.apply(inst, arguments);
            }
            // call init if there is an init, if setup returned args, use those as the arguments
            if (inst.init) {
                inst.init.apply(inst, isArray(args) ? args : arguments);
            }

            return inst;
        },

        setup: function(baseClass, fullName) {
            // set defaults as the merger of the parent defaults and this object's defaults
            this.defaults = extend(true, {}, baseClass.defaults, this.defaults);
            return arguments;
        },

        rawInstance: function() {
            // prevent running init
            initializing = true;
            var inst = new this();
            initializing = false;
            // allow running init
            return inst;
        },

        extend: function(fullName, klass, proto) {
            // figure out what was passed and normalize it
            if (typeof fullName !== 'string') {
                proto = klass;
                klass = fullName;
                fullName = null;
            }

            if (!proto) {
                proto = klass;
                klass = null;
            }

            proto = proto || {};
            var _super_class = this,
                _super = this[STR_PROTOTYPE],
                name, shortName, namespace, prototype;

            // Instantiate a base class (but only create the instance,
            // don't run the init constructor)
            initializing = true;
            prototype = new this();
            initializing = false;

            // Copy the properties over onto the new prototype
            inheritProps(proto, _super, prototype);

            // The dummy class constructor
            function Class() {
                // All construction is actually done in the init method
                if (initializing) {
                    return undefined;
                }

                // we are being called w/o new, we are extending
                if (this.constructor !== Class && arguments.length) {
                    //return Class.extend.apply(Class, arguments);
                    return arguments.callee.extend.apply(arguments.callee, arguments);
                } else { //we are being called w/ new
                    return this.Class.newInstance.apply(this.Class, arguments);
                }
            }

            // Copy old stuff onto class
            for (name in this) {
                if (this.hasOwnProperty(name)) {
                    Class[name] = this[name];
                }
            }

            // copy new static props on class
            inheritProps(klass, this, Class);

            // do namespace stuff
            if (fullName) {
                var parts = fullName.split(/\./),
                    shortName = parts.pop(),
                    current = getObject(parts.join('.'), window, true),
                    namespace = current;

                if (current[shortName]) {
                    throw ('baseClass.js There\'s already something called ' + fullName);
                }

                //!steal-remove-end
                current[shortName] = Class;
            }

            // set things that can't be overwritten
            extend(Class, {
                prototype: prototype,
                namespace: namespace,
                shortName: shortName,
                constructor: Class,
                fullName: fullName
            });

            //make sure our prototype looks nice
            Class[STR_PROTOTYPE].Class = Class[STR_PROTOTYPE].constructor = Class;

            // call the class setup
            var args = Class.setup.apply(Class, concatArgs([_super_class], arguments));

            // call the class init
            if (Class.init) {
                Class.init.apply(Class, args || concatArgs([_super_class], arguments));
            }

            /* @Prototype*/
            return Class;
        }
    });

    // proxy "this" context to the callback
    baseClass.callback = baseClass[STR_PROTOTYPE].callback = baseClass[STR_PROTOTYPE].proxy = baseClass.proxy;

    // export base class
    module.exports = baseClass;
})();