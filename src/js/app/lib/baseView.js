'use strict';

(function() {
    var baseView = function(viewName) {
        this.viewName = viewName;
    };
    baseView.prototype = {
        viewName: '',

        render: function(data) {
            return Mec.Template[this.viewName](data);
        }
    };

    module.exports = baseView;
})();