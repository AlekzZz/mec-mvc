'use strict';

(function() {
    var Class = require('./baseClass.js');

    /**
     * @class Controller
     * @inherits Base Class
     */
    var Controller = Class(
    /* @static */
    {},
    /* @prototype */
    {
        setup: function(options) {
            this.options = $.extend($.extend(true, {}, this.Class.defaults), options);

            return [this.scope, this.options].concat($.makeArray(arguments).slice(2));
        }
    });

    module.exports = Controller;
})();