'use strict';

(function() {
    var Class = require('lib/baseClass.js');
    require('models/product.js');

    Class('Mec.Services.Product',
    /* @Static */
    {
        defaults: {
            domain: 'http://www.mec.ca',
            apiPath: '/api/v1',
            url: '/products/search',
            model: Mec.Models.Product,
            contract: null
        },

        /**
         * Products lookup
         */
        findAll: function(params, success, error) {
            return this.resource(params, success, error);
        },

        /**
         * Make an ajax request and return a promise
         */
        resource: function(params, success, error) {
            var options = this.defaults;
            var url = options.domain + options.apiPath + options.url;

            // set contract for the resource
            this.contract = (params.contract ? params.contract : null);

            // merge params
            params = $.extend(true, {
                keywords: ''
            }, params);

            return $.ajax(url, {
                data: params
            }).then(this.callback('hydrate'));
        },

        /**
         * Hydrate json with model instances
         */
        hydrate: function(data) {
            var options = this.defaults;

            if (data && data.products) {
                var products = [];

                for (var i = 0, len = data.products.length; i < len; i++) {
                    var product = new options.model(data.products[i], {
                        contract: options.contract
                    });

                    products.push(product);
                }

                data.products = products;
            }

            return data;
        }
    },
    /* @Prototype */
    {});
})();