'use strict';

(function() {
    var Controller = require('lib/baseController.js');

    /**
     * @class Ui.Tabs
     * @inherits Controller
     */
    Controller('Ui.Tabs',
    /** @Static */
    {
        defaults: {
            selectors: {
                tab: '.ui-tab',
                content: '.ui-tab-content'
            },
            activeClass: 'active',
            onInit: function() {},
            onTabClick: function() {},
            onContentShow: function() {}
        }
    },
    /** @Prototype */
    {
        /**
         * Main Init
         */
        init: function() {
            var self = this;
            var options = self.options;
            var tab = self.scope.find(options.selectors.tab);
            var content = self.scope.find(options.selectors.content);

            // Trigger initial tab
            if (tab.filter('.' + options.activeClass).length === 0) {
                tab.filter(':first').addClass(options.activeClass).siblings().removeClass(options.activeClass);
                content.filter(':first').addClass(options.activeClass).siblings().removeClass(options.activeClass);
            } else {
                var index = tab.filter('.' + options.activeClass).index();
                content.eq(index).addClass(options.activeClass).siblings().removeClass(options.activeClass);
            }

            // Call onInit callback
            options.onInit(self.scope, options);

            // event listeners
            self.scope.on('click', options.selectors.tab, function(ev) {
                self.tabClickHandler($(this), ev);
            });
        },

        /**
         * Tab click handler
         */
        tabClickHandler: function(el, ev) {
            ev.preventDefault();

            var self = this;
            var options = self.options;

            // Call onTabClick callback
            options.onTabClick(el, ev);

            // Set active class for selected tab based on index
            el.addClass(options.activeClass).siblings().removeClass(options.activeClass);

            // Set active class for selected content based on tab index
            var content = self.scope.find(options.selectors.content).eq(el.index());
            self.contentShow(content);
        },

        /**
         * Content display
         */
        contentShow: function(el) {
            var self = this;
            var options = self.options;

            // Set active class for selected tab based on index
            el.addClass(options.activeClass).siblings().removeClass(options.activeClass);

            // Call onContentShow callback
            options.onContentShow(el);
        }
    });
})();