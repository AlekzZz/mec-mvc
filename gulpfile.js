'use strict';

var path = require('path');
var gulp = require('gulp');
var gutil = require('gulp-util');
var less = require('gulp-less');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var concatenate = require('gulp-concat');
var browserify = require('browserify');
var pathmodify = require('pathmodify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync').create();

var eslint = require('gulp-eslint');

// handlebars
var handlebars = require('gulp-handlebars');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');

/**
 * Server through browser sync
 */
gulp.task('browser-sync', function() {
    browserSync.init({
        server : {
            baseDir : './public'
        },
        open : false
    });
});

/**
 * less compile
 */
gulp.task('less', function() {
    gulp.src('src/less/main.less')
        .pipe(less())
        .pipe(minifyCSS())
        .pipe(rename('production.css'))
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.reload({ stream : true }));
});

/**
 * JS Pack for vendors
 */
gulp.task('js:vendors', function () {
    return gulp.src([
            './src/js/vendors/jquery/jquery.js',
            './src/js/vendors/handlebars/handlebars.js',
            './src/js/vendors/pubsub/pubsub.js'
        ])
        .pipe(concatenate('vendors.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./public/js'));
});

/**
 * JS Pack for main app
 */
gulp.task('js', function(callback) {
    return browserify({
            entries : [
                './src/js/app/app.js'
            ],
            debug : true,
            // watchify requires these options
            cache : {},
            packageCache : {},
            fullPaths : false
        }).plugin(pathmodify(), {
            mods : [
                pathmodify.mod.dir('app', path.join(__dirname, 'src/js/app')),
                pathmodify.mod.dir('lib', path.join(__dirname, 'src/js/app/lib')),
                pathmodify.mod.dir('searcher', path.join(__dirname, 'src/js/app/searcher')),
                pathmodify.mod.dir('ui', path.join(__dirname, 'src/js/app/ui')),
                pathmodify.mod.dir('models', path.join(__dirname, 'src/js/app/models')),
                pathmodify.mod.dir('services', path.join(__dirname, 'src/js/app/services')),
                pathmodify.mod.dir('views', path.join(__dirname, 'src/js/app/views'))
            ]
        })
        .on('error', function(err) {
            // print the error (can replace with gulp-util)
            console.log(err.message);
            // end this stream
            this.emit('end');
        })
        .bundle()
        .pipe(source('production.js'))
        //.pipe(uglify())
        .pipe(gulp.dest('./public/js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true })); // loads map from browserify file
});

gulp.task('lint', function () {
    return gulp.src(['src/js/**/*.js'])
        // eslint() attaches the lint output to the eslint property
        // of the file object so it can be used by other modules.
        .pipe(eslint())
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failOnError last.
        .pipe(eslint.failOnError());
});

gulp.task('templates', function () {
    gulp.src('./src/js/app/templates/**/*.hbs')
        .pipe(handlebars())
        .pipe(wrap('Handlebars.template(<%= contents %>)'))
        .pipe(declare({
            namespace : 'Mec.Template',
            noRedeclare : true, // Avoid duplicate declarations
        }))
        .pipe(concatenate('templates.js'))
        .pipe(gulp.dest('./public/js'));
});

/**
 * Main watch task
 */
gulp.task('watch', function() {
    gulp.watch('src/less/**/*.less', ['less']);
    gulp.watch('src/js/app/**/*.hbs', ['templates']);
    gulp.watch('src/js/app/**/*.js', ['js']);
    gulp.watch('src/js/vendors/**/*.js', ['js:vendors']);

    gulp.watch('public/index.html').on('change', browserSync.reload);
    gulp.watch('public/js/**/*.html').on('change', browserSync.reload);
    gulp.watch('public/js/**/*.js').on('change', browserSync.reload);
    gulp.watch('public/js/**/*.hbs').on('change', browserSync.reload);
});

/**
 * Main watch task
 */
gulp.task('dev', ['less', 'templates', 'js', 'watch', 'browser-sync', 'lint'], function() {});

/**
 * Default task
 */
gulp.task('default', ['js', 'watch'], function() {
    // execute watch by default
});